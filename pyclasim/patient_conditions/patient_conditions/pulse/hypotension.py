import logging

from pulse.cdm.patient_actions import SEHemorrhage, eHemorrhageType
from pulse.cpm.PulsePhysiologyEngine import PulsePhysiologyEngine

from patient_models import PatientCondition

logger = logging.getLogger(__name__)


class Hypotension(PatientCondition):
    def __init__(self) -> None:
        self.threshold_MAP: float = 70.0
        self.is_running: bool = True

    def initialize(self, pulse: PulsePhysiologyEngine) -> None:
        logger.info("Initializing hypotension condition")
        self.hemorrhage = SEHemorrhage()
        self.hemorrhage.set_comment("Laceration to the leg")
        self.hemorrhage.set_type(eHemorrhageType.External)
        self.hemorrhage.set_compartment("RightLeg")
        self.hemorrhage.get_severity().set_value(0.9)
        pulse.process_action(self.hemorrhage)

    def execute(self, timestep: int, pulse: PulsePhysiologyEngine) -> None:
        if not self.is_running:
            return

        results = pulse.pull_data()
        if results["MeanArterialPressure (mmHg)"][-1] <= self.threshold_MAP:
            logger.info(f"Patient's MAP has dropped to {self.threshold_MAP} mmHg")
            self.hemorrhage.get_severity().set_value(0)
            pulse.process_action(self.hemorrhage)
            self.is_running = False
