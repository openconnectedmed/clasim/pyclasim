from typing import List

from patient_conditions.pulse.hypotension import Hypotension

from patient_models import PatientCondition

__all__ = [
    "get_patient_conditions",
]


def get_patient_conditions() -> List[PatientCondition]:
    return [
        Hypotension(),
    ]
