from setuptools import find_packages, setup

setup(
    name="patient_conditions",
    version="0.0.1",
    packages=find_packages(include=["patient_conditions", "patient_conditions.*"]),
    install_requires=[
        "foundation",
        "patient_models",
    ],
    extras_requires={"dev": ["pytest"]},
    python_requires=">=3.8",
)
