from setuptools import find_packages, setup

setup(
    name="presentation",
    version="0.0.1",
    packages=find_packages(include=["presentation", "presentation.*"]),
    install_requires=[
        "injector",
        "marshmallow",
        "marshmallow-dataclass",
        "foundation",
        "messaging",
        "monitor",
        "pump",
    ],
    extras_requires={"dev": ["pytest"]},
    python_requires=">=3.8",
)
