import pytest

from monitor import CreateMonitorCommand
from presentation.message_loader import MessageLoader
from pump import GetPumpQuery
from pump.domain.events import PumpRateChanged


def test_successfully_loads_valid_query_dict():
    query_dict = {"query": "get_pump", "name": "pump"}

    query = MessageLoader.load(query_dict)

    assert query == GetPumpQuery(name="pump")


def test_successfully_loads_valid_command_dict():
    cmd_dict = {"action": "create_monitor", "name": "monitor"}

    cmd = MessageLoader.load(cmd_dict)

    assert cmd == CreateMonitorCommand(name="monitor")


def test_successfully_loads_valid_event_dict():
    event_dict = {"event_name": "pump_rate_changed", "rate": 50, "name": "pump"}

    event = MessageLoader.load(event_dict)

    assert event == PumpRateChanged(name="pump", rate=50)


@pytest.mark.parametrize(
    "invalid_msg_dict",
    [
        {},
        {"query": "get_pump"},
        {"query": "unknown_key"},
        {"action": "create_monitor"},
        {"event_name": "pump_rate_changed", "name": "pump"},
    ],
)
def test_fails_to_load_invalid_message_dict_missing_required_fields(
    invalid_msg_dict,
):
    message = MessageLoader.load(invalid_msg_dict)

    assert message is None
