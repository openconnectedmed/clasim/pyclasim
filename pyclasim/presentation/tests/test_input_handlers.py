import json
from unittest.mock import Mock

import pytest

from presentation.input_handler import InputHandler


@pytest.fixture
def mock_listener():
    listener = Mock()
    listener.receive.return_value = None
    return listener


def test_successfully_processes_input_given_a_valid_query(mock_listener):
    mock_message_bus = Mock()
    mock_message_bus.dispatch.return_value = {}
    mock_responder = Mock()
    mock_responder.receive.return_value = json.dumps(
        {"query": "get_pump", "name": "pump1"}
    )
    input_handler = InputHandler(
        listener=mock_listener,
        responder=mock_responder,
        message_bus=mock_message_bus,
        event_bus=Mock(),
    )

    input_handler.process_input()

    mock_message_bus.dispatch.assert_called()
    mock_responder.send.assert_called()


def test_successfully_processes_input_given_a_valid_command(mock_listener):
    mock_message_bus = Mock()
    mock_listener.receive.return_value = json.dumps(
        {"action": "create_monitor", "name": "monitor"}
    )
    input_handler = InputHandler(
        listener=mock_listener,
        responder=Mock(),
        message_bus=mock_message_bus,
        event_bus=Mock(),
    )

    input_handler.process_input()

    mock_message_bus.dispatch.assert_called()


def test_successfully_processes_input_given_a_valid_event(mock_listener):
    mock_listener.receive.return_value = json.dumps(
        {"event_name": "pump_rate_changed", "rate": 50, "name": "pump"}
    )
    mock_event_bus = Mock()
    input_handler = InputHandler(
        listener=mock_listener,
        responder=Mock(),
        message_bus=Mock(),
        event_bus=mock_event_bus,
    )

    input_handler.process_input()

    mock_event_bus.publish.assert_called()


@pytest.mark.parametrize(
    "invalid_msg",
    [
        {},
        {"query": "get_pump"},
        {"action": "create_monitor"},
    ],
)
def test_fails_process_input_given_invalid_command_or_query(mock_listener, invalid_msg):
    mock_message_bus = Mock()
    mock_listener.receive.return_value = json.dumps(invalid_msg)
    input_handler = InputHandler(
        listener=mock_listener,
        responder=Mock(),
        message_bus=mock_message_bus,
        event_bus=Mock(),
    )

    input_handler.process_input()

    mock_message_bus.dispatch.assert_not_called()


@pytest.mark.parametrize(
    "invalid_event",
    [
        {},
        {"event_name": "unknown_key"},
        {"event_name": "pump_rate_changed"},
    ],
)
def test_fails_process_input_given_invalid_event(mock_listener, invalid_event):
    mock_listener.receive.return_value = json.dumps(invalid_event)
    mock_event_bus = Mock()
    input_handler = InputHandler(
        listener=mock_listener,
        responder=Mock(),
        message_bus=Mock(),
        event_bus=mock_event_bus,
    )

    input_handler.process_input()

    mock_event_bus.publish.assert_not_called()
