import json
from dataclasses import asdict, dataclass

from messaging import MessagingPublisher

from monitor import UpdatingMonitorsOutputBoundary, UpdatingMonitorsOutputDto


@dataclass
class MonitorsPresenter(UpdatingMonitorsOutputBoundary):
    messaging_client: MessagingPublisher

    def present(self, dto: UpdatingMonitorsOutputDto) -> None:
        json_data = json.dumps(asdict(dto))
        self.messaging_client.publish(json_data)
