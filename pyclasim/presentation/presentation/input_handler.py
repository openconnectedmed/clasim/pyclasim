import json
import logging
from dataclasses import dataclass
from typing import Any, Dict, Optional, Union

from messaging import MessagingListener, MessagingResponder

from foundation.events import Event, EventBus
from foundation.messages import Command, MessageBus, Query

from presentation.message_loader import MessageLoader

logger = logging.getLogger(__name__)


@dataclass
class InputHandler:
    listener: MessagingListener
    responder: MessagingResponder
    message_bus: MessageBus
    event_bus: EventBus

    def process_input(self) -> None:
        message_dto = self._get_message_dto()
        self._handle_message(message_dto)

    def _get_message_dto(self) -> Optional[Union[Command, Query, Event]]:
        if not (message_dict := self._receive_input()):
            return

        if not (message_dto := MessageLoader.load(message_dict)):
            return

        return message_dto

    def _handle_message(self, message_dto: Union[Command, Query, Event]) -> None:
        if isinstance(message_dto, Command):
            self._handle_command(message_dto)

        elif isinstance(message_dto, Query):
            self._handle_query(message_dto)

        elif isinstance(message_dto, Event):
            self._handle_event(message_dto)

    def _handle_command(self, command: Command) -> None:
        self.message_bus.dispatch(command)

    def _handle_query(self, query: Query) -> None:
        response = self.message_bus.dispatch(query)
        self.responder.send(json.dumps(response))

    def _handle_event(self, event: Event) -> None:
        self.event_bus.publish([event])

    def _receive_input(self) -> Optional[Dict[str, Any]]:
        if not (msg_json := self.listener.receive() or self.responder.receive()):
            return
        return json.loads(msg_json)
