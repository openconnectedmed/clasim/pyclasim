import logging
from typing import Any, Dict, Optional, Type, Union

import marshmallow_dataclass
from marshmallow import EXCLUDE, ValidationError

from foundation.events import Event
from foundation.messages import Command, Query

from monitor import (
    ChangeMonitorOutputPeriodCommand,
    CreateMonitorCommand,
    FilterMonitorOutputCommand,
)
from pump import (
    CreatePumpCommand,
    GetAllPumpsQuery,
    GetPumpQuery,
    PausePumpCommand,
    RemovePumpCommand,
    SetPumpRateCommand,
    StartPumpCommand,
    StopPumpCommand,
    UnpausePumpCommand,
)
from pump.domain.events import (
    PumpConnected,
    PumpPaused,
    PumpRateChanged,
    PumpStopped,
    PumpUnpaused,
)

logger = logging.getLogger(__name__)


class MessageLoader:
    messages: Dict[str, Union[Type[Event], Type[Command], Type[Query]]] = {
        # monitor commands
        "create_monitor": CreateMonitorCommand,
        "change_output_period": ChangeMonitorOutputPeriodCommand,
        "filter_output": FilterMonitorOutputCommand,
        # pump comands
        "create_pump": CreatePumpCommand,
        "start_pump": StartPumpCommand,
        "set_pump_rate": SetPumpRateCommand,
        "pause_pump": PausePumpCommand,
        "unpause_pump": UnpausePumpCommand,
        "stop_pump": StopPumpCommand,
        "remove_pump": RemovePumpCommand,
        # pump queries
        "get_pump": GetPumpQuery,
        "get_pumps": GetAllPumpsQuery,
        # events
        "pump_connected": PumpConnected,
        "pump_rate_changed": PumpRateChanged,
        "pump_paused": PumpPaused,
        "pump_unpaused": PumpUnpaused,
        "pump_stopped": PumpStopped,
    }

    @staticmethod
    def _extract_message_key(msg_dict: Dict[str, Any]) -> str:
        if "action" in msg_dict:
            return msg_dict["action"]

        if "query" in msg_dict:
            return msg_dict["query"]

        if "event_name" in msg_dict:
            return msg_dict["event_name"]

        raise KeyError(
            "Message expected to contain one of the following keys: "
            "'action', 'query' and 'event_name'"
        )

    @classmethod
    def load(cls, msg_dict: Dict[str, Any]) -> Optional[Union[Command, Query, Event]]:
        try:
            message_key = cls._extract_message_key(msg_dict)
            schema_cls = marshmallow_dataclass.class_schema(cls.messages[message_key])
            return schema_cls().load(msg_dict, unknown=EXCLUDE)

        except KeyError as e:
            logger.error(f"Unknown request: {e}")
        except ValidationError as e:
            logger.error(f"Couldn't load command from input, {e.messages}")
        except Exception as e:
            logger.error(f"Couldn't handle input, {e}")
