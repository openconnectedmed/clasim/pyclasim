from injector import Module, provider
from messaging import MessagingListener, MessagingPublisher, MessagingResponder

from foundation.events import EventBus
from foundation.messages import MessageBus

from monitor import UpdatingMonitorsOutputBoundary
from presentation.input_handler import InputHandler
from presentation.presenters import MonitorsPresenter

__all__ = [
    "InputHandler",
    "PresentationModule",
]


class PresentationModule(Module):
    @provider
    def provide_command_input_handler(
        self,
        listener: MessagingListener,
        responder: MessagingResponder,
        message_bus: MessageBus,
        event_bus: EventBus,
    ) -> InputHandler:
        return InputHandler(listener, responder, message_bus, event_bus)

    @provider
    def provide_monitors_presenter(
        self, messaging_client: MessagingPublisher
    ) -> UpdatingMonitorsOutputBoundary:
        return MonitorsPresenter(messaging_client)
