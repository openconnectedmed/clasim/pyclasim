from setuptools import find_packages, setup

setup(
    name="infrastructure",
    version="0.0.1",
    packages=find_packages(include=["infrastructure", "infrastructure.*"]),
    install_requires=[
        "messaging",
    ],
    python_requires=">=3.8",
)
