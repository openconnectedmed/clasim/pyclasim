import json
import logging
from dataclasses import dataclass
from typing import Any, Dict

from messaging import MessagingPublisher, MessagingRequester

logger = logging.getLogger(__name__)


@dataclass
class SimulatedPumpApi:
    pump_id: str
    publisher: MessagingPublisher
    requester: MessagingRequester

    def start(self, rate: float) -> None:
        cmd = self._build_command("start_pump", rate=rate)
        self._send_command(cmd)
        logger.info(f"started {self.pump_id} with rate: {rate} mL/hr")

    def set_rate(self, rate: float) -> None:
        cmd = self._build_command("set_pump_rate", rate=rate)
        self._send_command(cmd)
        logger.info(f"changed {self.pump_id}'s rate to {rate} mL/hr")

    def stop(self) -> None:
        cmd = self._build_command("stop_pump")
        self._send_command(cmd)
        logger.info(f"stopped {self.pump_id}")

    def pause(self) -> None:
        cmd = self._build_command("pause_pump")
        self._send_command(cmd)
        logger.info(f"paused {self.pump_id}")

    def unpause(self) -> None:
        cmd = self._build_command("unpause_pump")
        self._send_command(cmd)
        logger.info(f"resumed {self.pump_id}")

    def get_pump_info(self, timeout: float = 1000) -> Dict[str, Any]:
        query = self._build_query("get_pump")
        self.requester.send(json.dumps(query))
        response = self.requester.receive(timeout=timeout)
        return json.loads(response)

    def _send_command(self, cmd: Dict[str, Any]) -> None:
        self.publisher.publish(json.dumps(cmd))

    def _build_command(self, action: str, **kwargs) -> Dict[str, Any]:
        cmd = {"action": action, "name": self.pump_id, **kwargs}
        return cmd

    def _build_query(self, query: str) -> Dict[str, Any]:
        query = {"query": query, "name": self.pump_id}
        return query
