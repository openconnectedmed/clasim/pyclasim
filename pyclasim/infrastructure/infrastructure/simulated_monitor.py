import json
from typing import Dict, Optional

from messaging import MessagingListener


class SimulatedMonitorApi:
    def __init__(self, monitor_id: str, listener: MessagingListener) -> None:
        self.monitor_id = monitor_id
        self.listener = listener

    def get_patient_data(self) -> Optional[Dict[str, float]]:
        if not (data := self.listener.receive()):
            return

        d = json.loads(data)
        return d.get("output") if d.get("name") == self.monitor_id else None
