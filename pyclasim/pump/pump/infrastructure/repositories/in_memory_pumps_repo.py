import functools
from dataclasses import dataclass, field
from typing import Callable, Dict, List

from foundation.events import EventBus

from pump.application.exceptions import PumpNotFoundError
from pump.application.repositories import PumpsRepository
from pump.domain.entities import Pump


@dataclass(repr=False)
class InMemoryPumpsRepo(PumpsRepository):
    event_bus: EventBus
    pumps: Dict[str, Pump] = field(init=False, default_factory=dict)

    def _publishes_events(func) -> Callable:
        @functools.wraps(func)
        def wrapper(self, pump: Pump) -> None:
            self.event_bus.publish([e for e in pump.events])
            func(self, pump)

        return wrapper

    def get_pump_by_name(self, name: str) -> Pump:
        if not self.pumps.get(name):
            raise PumpNotFoundError(name)

        return self.pumps.get(name)

    @_publishes_events
    def save(self, pump: Pump) -> None:
        self.pumps[pump.name] = pump

    @_publishes_events
    def remove(self, pump: Pump) -> None:
        del self.pumps[pump.name]

    def get_all_pumps(self) -> List[Pump]:
        return list(self.pumps.values())
