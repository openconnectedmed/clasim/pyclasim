from dataclasses import dataclass
from typing import Any, Dict, List

from pump.application.queries.base import PumpQuery, QueryHandler


@dataclass(frozen=True)
class GetAllPumpsQuery(PumpQuery):
    pass


@dataclass
class GetAllPumpsHandler(QueryHandler):
    def __call__(self, query: PumpQuery) -> List[Dict[str, Any]]:
        if pumps := self.repo.get_all_pumps():
            return [
                {
                    "name": pump.name,
                    "status": pump.state.value,
                    "rate": pump.rate.value,
                    "fluid": pump.fluid,
                }
                for pump in pumps
            ]

        return []
