from dataclasses import dataclass
from typing import Any, Dict

from pump.application.exceptions import PumpNotFoundError
from pump.application.queries.base import PumpQuery, QueryHandler


@dataclass(frozen=True)
class GetPumpQuery(PumpQuery):
    name: str


@dataclass
class GetPumpHandler(QueryHandler):
    def __call__(self, query: PumpQuery) -> Dict[str, Any]:
        try:
            pump = self.repo.get_pump_by_name(query.name)

        except PumpNotFoundError:
            return {}

        else:
            return {
                "name": pump.name,
                "status": pump.state.value,
                "rate": pump.rate.value,
                "fluid": pump.fluid,
            }
