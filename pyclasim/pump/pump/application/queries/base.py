from dataclasses import dataclass

from foundation.messages import Query

from pump.application.repositories import PumpsRepository


class PumpQuery(Query):
    pass


@dataclass
class QueryHandler:
    repo: PumpsRepository
