class PumpNotFoundError(Exception):
    def __init__(self, name: str) -> None:
        self.message = f"'{name}' does not exist"
        super().__init__(self.message)
