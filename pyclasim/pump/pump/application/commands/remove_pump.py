import logging
from dataclasses import dataclass

from pump.application.commands.base import CommandHandler, PumpCommand
from pump.application.exceptions import PumpNotFoundError

logger = logging.getLogger("pump.application.commands")


@dataclass(frozen=True)
class RemovePumpCommand(PumpCommand):
    name: str


class RemovePumpHandler(CommandHandler):
    def __call__(self, cmd: PumpCommand) -> None:
        try:
            pump = self.repo.get_pump_by_name(cmd.name)

        except PumpNotFoundError:
            logger.info(f"You are trying to remove pump: {cmd.name} that doesn't exist")

        else:
            pump.stop()
            self.repo.remove(pump)
            logger.info(f"{cmd.name} has been removed")
