from dataclasses import dataclass

from foundation.messages import Command

from pump.application.repositories import PumpsRepository


class PumpCommand(Command):
    pass


@dataclass
class CommandHandler:
    repo: PumpsRepository
