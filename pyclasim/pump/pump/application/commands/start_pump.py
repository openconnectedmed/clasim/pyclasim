from dataclasses import dataclass

from pump.application.commands.base import CommandHandler, PumpCommand
from pump.application.exceptions import PumpNotFoundError
from pump.domain.value_objects import Rate


@dataclass(frozen=True)
class StartPumpCommand(PumpCommand):
    name: str
    rate: float


class StartPumpHandler(CommandHandler):
    def __call__(self, cmd: PumpCommand) -> None:
        try:
            pump = self.repo.get_pump_by_name(cmd.name)
            pump.start(Rate(cmd.rate))
            self.repo.save(pump)

        except PumpNotFoundError:
            raise
