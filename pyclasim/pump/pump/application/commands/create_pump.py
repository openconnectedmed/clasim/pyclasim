import logging
from dataclasses import dataclass
from typing import Optional

from foundation.csv_logger import CSVLogger

from pump.application.commands.base import CommandHandler, PumpCommand
from pump.application.config import PumpConfig
from pump.application.exceptions import PumpNotFoundError
from pump.domain.entities import Pump

logger = logging.getLogger("pump.application.commands")


@dataclass(frozen=True)
class CreatePumpCommand(PumpCommand):
    name: str
    fluid: str
    bag_volume: Optional[float] = None
    concentration: Optional[float] = None


@dataclass
class CreatePumpHandler(CommandHandler):
    config: PumpConfig

    def __call__(self, cmd: PumpCommand) -> None:
        try:
            self.repo.get_pump_by_name(cmd.name)

        except PumpNotFoundError:
            pump = Pump(
                name=cmd.name,
                fluid=cmd.fluid,
                bag_volume=cmd.bag_volume,
                concentration=cmd.concentration,
                csv_logger=CSVLogger(self.config.results_dir, f"{cmd.name}.csv"),
            )
            self.repo.save(pump)
            logger.info(f"'{cmd.name}' has been created")

        else:
            logger.error(f"Can't create '{cmd.name}', it already exits")
