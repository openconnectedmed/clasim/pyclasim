from dataclasses import dataclass

from pump.application.commands.base import CommandHandler, PumpCommand
from pump.application.exceptions import PumpNotFoundError


@dataclass(frozen=True)
class PausePumpCommand(PumpCommand):
    name: str


class PausePumpHandler(CommandHandler):
    def __call__(self, cmd: PumpCommand) -> None:
        try:
            pump = self.repo.get_pump_by_name(cmd.name)
            pump.pause()
            self.repo.save(pump)

        except PumpNotFoundError:
            raise
