import abc
from typing import List

from pump.domain.entities import Pump


class PumpsRepository(abc.ABC):
    @abc.abstractmethod
    def get_pump_by_name(self, name: str) -> Pump:
        pass

    @abc.abstractmethod
    def save(self, pump: Pump) -> None:
        pass

    @abc.abstractmethod
    def remove(self, pump: Pump) -> None:
        pass

    @abc.abstractmethod
    def get_all_pumps(self) -> List[Pump]:
        pass
