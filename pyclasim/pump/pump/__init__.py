from typing import Callable, Dict, Type, Union

from injector import Module, multiprovider, provider, singleton

from foundation.events import EventBus
from foundation.messages import Command, Query

from pump.application.commands import (
    CreatePumpCommand,
    CreatePumpHandler,
    PausePumpCommand,
    PausePumpHandler,
    RemovePumpCommand,
    RemovePumpHandler,
    SetPumpRateCommand,
    SetPumpRateHandler,
    StartPumpCommand,
    StartPumpHandler,
    StopPumpCommand,
    StopPumpHandler,
    UnpausePumpCommand,
    UnpausePumpHandler,
)
from pump.application.config import PumpConfig
from pump.application.queries import (
    GetAllPumpsHandler,
    GetAllPumpsQuery,
    GetPumpHandler,
    GetPumpQuery,
)
from pump.application.repositories import PumpsRepository
from pump.domain.events import (
    PumpConnected,
    PumpPaused,
    PumpRateChanged,
    PumpStopped,
    PumpUnpaused,
)
from pump.infrastructure.repositories import InMemoryPumpsRepo

__all__ = [
    "PulseConfig",
    "PumpModule",
    "PumpInfrastructureModule",
    # events
    "PumpConnected",
    "PumpPaused",
    "PumpRateChanged",
    "PumpStopped",
    "PumpUnpaused",
    # commands
    "CreatePumpCommand",
    "StartPumpCommand",
    "StopPumpCommand",
    "PausePumpCommand",
    "UnpausePumpCommand",
    "RemovePumpCommand",
    "SetPumpRateCommand",
    # queries
    "GetAllPumpsQuery",
    "GetPumpQuery",
]


class PumpModule(Module):
    @multiprovider
    def provide_pump_handlers(
        self, repo: PumpsRepository, config: PumpConfig
    ) -> Dict[Union[Type[Command], Type[Query]], Callable]:
        return {
            CreatePumpCommand: CreatePumpHandler(repo, config),
            StartPumpCommand: StartPumpHandler(repo),
            StopPumpCommand: StopPumpHandler(repo),
            PausePumpCommand: PausePumpHandler(repo),
            UnpausePumpCommand: UnpausePumpHandler(repo),
            SetPumpRateCommand: SetPumpRateHandler(repo),
            RemovePumpCommand: RemovePumpHandler(repo),
            GetPumpQuery: GetPumpHandler(repo),
            GetAllPumpsQuery: GetAllPumpsHandler(repo),
        }


class PumpInfrastructureModule(Module):
    @singleton
    @provider
    def pumps_repo(self, event_bus: EventBus) -> PumpsRepository:
        return InMemoryPumpsRepo(event_bus)
