from dataclasses import dataclass


@dataclass(order=True, frozen=True)
class Rate:
    value: [int, float]

    def __post_init__(self):
        if not isinstance(self.value, (int, float)):
            raise TypeError(
                "expects rate to be either float or int, not "
                f"{self.value.__class__.__name__}"
            )

        if self.value < 0:
            raise ValueError("rate cannot be negative")
