import functools
import logging
from enum import Enum
from typing import Callable, List

from foundation.csv_logger import CSVLogger
from foundation.events import Event

from pump.domain.events import (
    EventMixin,
    PumpConnected,
    PumpPaused,
    PumpRateChanged,
    PumpStopped,
    PumpUnpaused,
)
from pump.domain.value_objects import Rate

logger = logging.getLogger(__name__)


def records_action_data(func: Callable[..., None]) -> Callable[..., None]:
    @functools.wraps(func)
    def wrapper(instance, *args, **kwargs) -> None:
        func_name = func.__name__
        if func_name == "rate":
            func_name = "setrate"

        if func_name == "__init__":
            func_name = "connect"

        func(instance, *args, **kwargs)

        instance._csv_logger.record_data_to_file(
            {
                "action": func_name,
                "rate": instance._rate.value,
                "status": instance._state.value,
            }
        )

    return wrapper


class Pump(EventMixin):
    class State(Enum):
        IDLE = "idle"
        RUNNING = "running"
        PAUSED = "paused"

    @records_action_data
    def __init__(
        self,
        name: str,
        fluid: str,
        csv_logger: CSVLogger,
        bag_volume: float = None,
        concentration: float = None,
    ) -> None:
        self._name = name
        self._fluid = fluid
        self._bag_volume = bag_volume
        self._concentration = concentration
        self._csv_logger = csv_logger

        self._state = self.State.IDLE
        self._rate: Rate = Rate(0)
        self._events: List[Event] = []

        self._validate_inputs(self._bag_volume, self._concentration)
        self._record_event(
            PumpConnected(
                self._name,
                self._fluid,
                self._bag_volume,
                self._concentration,
            )
        )

    @property
    def state(self):
        return self._state

    @property
    def rate(self) -> Rate:
        return self._rate

    @rate.setter
    @records_action_data
    def rate(self, rate: Rate) -> None:
        if not self._state == self.State.RUNNING:
            return

        if rate == self._rate:
            return

        self._rate = rate
        self._record_event(PumpRateChanged(self._name, self._rate.value))
        logger.info(f"'{self._name}'s rate changed to {self._rate.value} mL/hr")

    @property
    def name(self) -> str:
        return self._name

    @property
    def fluid(self) -> str:
        return self._fluid

    @records_action_data
    def start(self, rate: Rate) -> None:
        if not self._state == self.State.IDLE:
            return

        self._state = self.State.RUNNING
        self._rate = rate
        self._record_event(PumpRateChanged(self._name, self._rate.value))
        logger.info(f"'{self._name}' has started running")

    @records_action_data
    def pause(self) -> None:
        if not self._state == self.State.RUNNING:
            return

        self._state = self.State.PAUSED
        self._record_event(PumpPaused(self._name))
        logger.info(f"'{self._name}' has been paused")

    @records_action_data
    def unpause(self) -> None:
        if not self._state == self.State.PAUSED:
            return

        self._state = self.State.RUNNING
        self._record_event(PumpUnpaused(self._name, self._rate.value))
        logger.info(f"'{self._name}' has resumed running")

    @records_action_data
    def stop(self) -> None:
        if self._state == self.State.IDLE:
            return

        self._state = self.State.IDLE
        self._rate = Rate(0)
        self._record_event(PumpStopped(self._name))
        logger.info(f"'{self._name}' has been stopped")

    @staticmethod
    def _validate_inputs(bag_volume: float, concentration: float) -> None:
        if all([bag_volume, concentration]):
            raise ValueError(
                "You are not allowed to provide both 'bag_volume' and "
                "'concentration' at the same time",
                bag_volume,
                concentration,
            )

        if not any([bag_volume, concentration]):
            raise ValueError(
                "Your are required to provide either 'bag_volume' or "
                "'concentration'",
                bag_volume,
                concentration,
            )

    def __eq__(self, other: "Pump") -> bool:
        if not isinstance(other, Pump):
            return False
        return self._name == other.name
