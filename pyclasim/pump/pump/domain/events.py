from dataclasses import dataclass
from typing import Iterator

from foundation.events import Event


@dataclass(frozen=True)
class PumpConnected(Event):
    name: str
    fluid: str
    bag_volume: float = None
    concentration: float = None


@dataclass(frozen=True)
class PumpPaused(Event):
    name: str
    rate: float = 0


@dataclass(frozen=True)
class PumpUnpaused(Event):
    name: str
    rate: float


@dataclass(frozen=True)
class PumpRateChanged(Event):
    name: str
    rate: float


@dataclass(frozen=True)
class PumpStopped(Event):
    name: str
    rate: float = 0


class EventMixin:
    @property
    def events(self) -> Iterator[Event]:
        return (self._events.pop(0) for _ in range(len(self._events)))

    def _record_event(self, event: Event) -> None:
        self._events.append(event)
