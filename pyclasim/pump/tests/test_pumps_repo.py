from unittest.mock import Mock

import pytest

from pump.application.exceptions import PumpNotFoundError
from pump.domain.entities import Pump
from pump.domain.events import PumpConnected
from pump.infrastructure.repositories import InMemoryPumpsRepo


@pytest.fixture()
def event_bus_mock():
    return Mock()


@pytest.fixture()
def pumps_repo(event_bus_mock):
    return InMemoryPumpsRepo(event_bus_mock)


def test_repo_raises_exception_if_pump_not_found(pumps_repo):

    with pytest.raises(PumpNotFoundError):
        pumps_repo.get_pump_by_name("pump")


def test_repo_returns_stored_pump_if_it_exists(pumps_repo):
    pump = Pump(name="pump", fluid="Saline", bag_volume=50, csv_logger=Mock())
    pumps_repo.save(pump)

    saved_pump = pumps_repo.get_pump_by_name("pump")

    assert saved_pump == pump


def test_repo_returns_all_stored_pumps(pumps_repo):
    pump1 = Pump(name="pump1", fluid="Saline", bag_volume=50, csv_logger=Mock())
    pump2 = Pump(name="pump2", fluid="Saline", bag_volume=50, csv_logger=Mock())
    pumps_repo.save(pump1)
    pumps_repo.save(pump2)

    pumps = pumps_repo.get_all_pumps()

    assert len(pumps) == 2
    assert pump1 in pumps
    assert pump2 in pumps


def test_repo_saves_pump_and_publishes_event(pumps_repo, event_bus_mock):
    pump = Pump(name="pump", fluid="Saline", bag_volume=50, csv_logger=Mock())

    pumps_repo.save(pump)

    assert len(pumps_repo.get_all_pumps()) == 1
    event_bus_mock.publish.assert_called_with([PumpConnected("pump", "Saline", 50)])


def test_repo_removes_pump_and_publishes_event(pumps_repo, event_bus_mock):
    pump = Pump(name="pump", fluid="Saline", bag_volume=50, csv_logger=Mock())
    pumps_repo.save(pump)

    pumps_repo.remove(pump)

    assert len(pumps_repo.get_all_pumps()) == 0
    event_bus_mock.publish.assert_called_with([])
