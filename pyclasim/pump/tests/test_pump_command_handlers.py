import tempfile
from unittest.mock import Mock

import pytest

import pump.application.commands as cmds
from pump.application.exceptions import PumpNotFoundError
from pump.domain.entities import Pump
from pump.domain.value_objects import Rate


class FakePumpConfig:
    @property
    def results_dir(self) -> str:
        return tempfile.mkdtemp()


@pytest.fixture()
def mock_pumps_repo():
    mock_repo = Mock()
    test_pump = Pump(name="pump", fluid="Saline", bag_volume=50, csv_logger=Mock())
    mock_repo.get_pump_by_name.return_value = test_pump
    return mock_repo


@pytest.mark.parametrize(
    "invalid_create_cmd",
    [
        (cmds.CreatePumpCommand("new_pump", "Norepinephrine", 60, 16)),
        (cmds.CreatePumpCommand("new_pump", "Norepinephrine")),
    ],
)
def test_handler_raises_exception_if_command_is_invalid(
    mock_pumps_repo, invalid_create_cmd
):
    mock_pumps_repo.get_pump_by_name.side_effect = PumpNotFoundError("")

    handler = cmds.CreatePumpHandler(repo=mock_pumps_repo, config=FakePumpConfig())

    with pytest.raises(ValueError):
        handler(invalid_create_cmd)


def test_handler_creates_new_pump_and_saves_it_to_repo_if_it_doesnot_exist_already(
    mock_pumps_repo,
):
    mock_pumps_repo.get_pump_by_name.side_effect = PumpNotFoundError("")
    create_cmd = cmds.CreatePumpCommand("new_pump", "Norepinephrine", concentration=16)

    handler = cmds.CreatePumpHandler(repo=mock_pumps_repo, config=FakePumpConfig())
    handler(create_cmd)

    mock_pumps_repo.save.assert_called_once()


def test_handler_runs_pump(mock_pumps_repo):
    run_cmd = cmds.StartPumpCommand("pump", 100)

    handler = cmds.StartPumpHandler(mock_pumps_repo)
    handler(run_cmd)

    pump = mock_pumps_repo.get_pump_by_name("pump")
    assert pump.rate == Rate(100)
    assert pump.state == Pump.State.RUNNING


def test_handler_stops_running_pump(mock_pumps_repo):
    cmds.StartPumpHandler(mock_pumps_repo)(cmds.StartPumpCommand("pump", 100))
    stop_cmd = cmds.StopPumpCommand("pump")

    handler = cmds.StopPumpHandler(mock_pumps_repo)
    handler(stop_cmd)

    pump = mock_pumps_repo.get_pump_by_name("pump")
    assert pump.rate == Rate(0)
    assert pump.state == Pump.State.IDLE


def test_handler_pauses_running_pump(mock_pumps_repo):
    cmds.StartPumpHandler(mock_pumps_repo)(cmds.StartPumpCommand("pump", 100))
    pause_cmd = cmds.StopPumpCommand("pump")

    handler = cmds.PausePumpHandler(mock_pumps_repo)
    handler(pause_cmd)

    pump = mock_pumps_repo.get_pump_by_name("pump")
    assert pump.rate == Rate(100)
    assert pump.state == Pump.State.PAUSED


def test_handler_unpauses_pump(mock_pumps_repo):
    cmds.StartPumpHandler(mock_pumps_repo)(cmds.StartPumpCommand("pump", 100))
    cmds.PausePumpHandler(mock_pumps_repo)(cmds.PausePumpCommand("pump"))
    unpause_cmd = cmds.UnpausePumpCommand("pump")

    handler = cmds.UnpausePumpHandler(mock_pumps_repo)
    handler(unpause_cmd)

    pump = mock_pumps_repo.get_pump_by_name("pump")
    assert pump.rate == Rate(100)
    assert pump.state == Pump.State.RUNNING


def test_handler_changes_running_pump_rate(mock_pumps_repo):
    cmds.StartPumpHandler(mock_pumps_repo)(cmds.StartPumpCommand("pump", 100))
    change_rate_cmd = cmds.SetPumpRateCommand("pump", 35)

    handler = cmds.SetPumpRateHandler(mock_pumps_repo)
    handler(change_rate_cmd)

    pump = mock_pumps_repo.get_pump_by_name("pump")
    assert pump.rate == Rate(35)
    assert pump.state == Pump.State.RUNNING


def test_handler_removes_pump(mock_pumps_repo):
    remove_cmd = cmds.RemovePumpCommand("pump")

    handler = cmds.RemovePumpHandler(mock_pumps_repo)
    handler(remove_cmd)

    mock_pumps_repo.remove.assert_called_once()


def test_handler_cannot_remove_pump_that_doesnot_exist(mock_pumps_repo):
    mock_pumps_repo.get_pump_by_name.side_effect = PumpNotFoundError("")
    remove_cmd = cmds.RemovePumpCommand("pump")

    handler = cmds.RemovePumpHandler(mock_pumps_repo)
    handler(remove_cmd)

    mock_pumps_repo.remove.assert_not_called()
