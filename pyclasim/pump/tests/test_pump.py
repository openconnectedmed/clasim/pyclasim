from unittest.mock import Mock

import pytest

from pump.domain.entities import Pump
from pump.domain.events import (
    PumpConnected,
    PumpPaused,
    PumpRateChanged,
    PumpStopped,
    PumpUnpaused,
)
from pump.domain.value_objects import Rate


@pytest.fixture
def pump():
    return Pump(name="pump1", fluid="Saline", bag_volume=50, csv_logger=Mock())


@pytest.fixture
def running_pump(pump):
    pump.start(Rate(100))
    return pump


def get_emitted_event(_pump):
    return list(_pump.events)[-1]


def test_creation_emits_event(pump):
    assert get_emitted_event(pump) == PumpConnected("pump1", "Saline", 50)


def test_creation_raises_exception_if_both_bag_vol_and_conc_are_provided():
    with pytest.raises(ValueError):
        Pump(
            name="pump1",
            fluid="Saline",
            bag_volume=50,
            concentration=80,
            csv_logger=Mock(),
        )


def test_creation_raises_exception_if_neither_bag_vol_nor_conc_is_provided():
    with pytest.raises(ValueError):
        Pump(name="pump1", fluid="Saline", csv_logger=Mock())


def test_cannot_run_pump_if_its_already_running(running_pump):
    running_pump.start(Rate(50))

    assert running_pump.rate == Rate(100)


def test_raises_exception_if_set_rate_value__lt__zero(pump):
    with pytest.raises(ValueError):
        pump.rate = Rate(-1)


def test_cannot_change_rate_if_pump_is_not_running(pump):
    pump.rate = Rate(50)

    assert pump.rate == Rate(0)
    assert pump.state == Pump.State.IDLE


def test_can_change_rate_if_pump_running(running_pump):
    running_pump.rate = Rate(50)

    assert running_pump.rate == Rate(50)
    assert get_emitted_event(running_pump) == PumpRateChanged("pump1", 50)


def test_can_run_pump(pump):
    assert pump.rate == Rate(0)
    assert pump.state == Pump.State.IDLE

    pump.start(Rate(150))

    assert pump.rate == Rate(150)
    assert pump.state == Pump.State.RUNNING
    assert get_emitted_event(pump) == PumpRateChanged("pump1", 150)


def test_can_pause_and_unpause_running_pump(running_pump):
    running_pump.pause()

    assert running_pump.state == Pump.State.PAUSED
    assert get_emitted_event(running_pump) == PumpPaused("pump1")

    running_pump.unpause()

    assert running_pump.rate == Rate(100)
    assert running_pump.state == Pump.State.RUNNING
    assert get_emitted_event(running_pump) == PumpUnpaused("pump1", 100)


def test_can_stop_running_pump(running_pump):
    running_pump.stop()

    assert running_pump.rate == Rate(0)
    assert running_pump.state == Pump.State.IDLE
    assert get_emitted_event(running_pump) == PumpStopped("pump1")
