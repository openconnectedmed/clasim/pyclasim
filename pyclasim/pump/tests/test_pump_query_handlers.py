from unittest.mock import Mock

from pump.application.exceptions import PumpNotFoundError
from pump.application.queries import (
    GetAllPumpsHandler,
    GetAllPumpsQuery,
    GetPumpHandler,
    GetPumpQuery,
)
from pump.domain.entities import Pump


def test_handler_returns_pump_given_name():
    mock_repo = Mock()
    mock_repo.get_pump_by_name.return_value = Pump(
        name="pump", fluid="Saline", bag_volume=50, csv_logger=Mock()
    )
    query = GetPumpQuery(name="pump")

    handler = GetPumpHandler(mock_repo)
    pump = handler(query)

    assert pump == {"name": "pump", "status": "idle", "rate": 0, "fluid": "Saline"}


def test_handler_returns_empty_dict_if_pump_not_found():
    mock_repo = Mock()
    mock_repo.get_pump_by_name.side_effect = PumpNotFoundError("")
    query = GetPumpQuery(name="pump")

    handler = GetPumpHandler(mock_repo)
    pump = handler(query)

    assert pump == {}


def test_handler_returns_all_pumps():
    mock_repo = Mock()
    mock_repo.get_all_pumps.return_value = [
        Pump(name="pump", fluid="Saline", bag_volume=50, csv_logger=Mock()),
        Pump(name="pump2", fluid="Blood", bag_volume=100, csv_logger=Mock()),
    ]
    query = GetAllPumpsQuery()

    handler = GetAllPumpsHandler(mock_repo)
    pumps = handler(query)

    assert pumps == [
        {"name": "pump", "status": "idle", "rate": 0, "fluid": "Saline"},
        {"name": "pump2", "status": "idle", "rate": 0, "fluid": "Blood"},
    ]


def test_handler_returns_empty_list_if_no_active_pumps():
    mock_repo = Mock()
    mock_repo.get_all_pumps.return_value = []
    query = GetAllPumpsQuery()

    handler = GetAllPumpsHandler(mock_repo)
    pumps = handler(query)

    assert pumps == []
