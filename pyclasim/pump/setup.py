from setuptools import find_packages, setup

setup(
    name="pump",
    version="0.0.1",
    packages=find_packages(include=["pump", "pump.*"]),
    install_requires=[
        "injector",
        "foundation",
    ],
    extras_requires={"dev": ["pytest"]},
    python_requires=">=3.8",
)
