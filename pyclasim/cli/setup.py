from setuptools import find_packages, setup

setup(
    name="cli",
    version="0.0.1",
    packages=find_packages(include=["cli", "cli.*"]),
    install_requires=[
        "click",
        "foundation",
        "messaging",
        "algorithms",
        "infrastructure",
        "patient_conditions",
        "main",
    ],
    python_requires=">=3.8",
)
