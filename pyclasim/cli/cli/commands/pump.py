import json

import click
from messaging import MessagingPublisher, MessagingRequester


def send_command(publisher: MessagingPublisher, action: str, **kwargs: dict) -> None:
    cmd = {"action": action, **kwargs}
    publisher.publish(json.dumps(cmd))


def send_query(requester: MessagingRequester, timeout: int, **kwargs: dict) -> str:
    query = {**kwargs}
    requester.send(json.dumps(query))

    return requester.receive(timeout=timeout)


@click.command()
@click.argument("name")
@click.option("-f", "--fluid", type=str)
@click.option("--bag-volume", type=float, default=None)
@click.option("--concentration", type=float, default=None)
@click.pass_obj
def create_pump(obj, **kwargs) -> None:
    """
    Creates a new pump instance
    """
    send_command(obj["publisher"], "create_pump", **kwargs)


@click.command()
@click.argument("name")
@click.option("--rate", type=float, required=True)
@click.pass_obj
def set_pump_rate(obj, **kwargs) -> None:
    """
    Set rate for running pump
    """
    send_command(obj["publisher"], "set_pump_rate", **kwargs)


@click.command()
@click.argument("name")
@click.option("--rate", type=float, required=True)
@click.pass_obj
def start_pump(obj, **kwargs) -> None:
    """
    Starts idle pump
    """
    send_command(obj["publisher"], "start_pump", **kwargs)


@click.command()
@click.argument("name")
@click.pass_obj
def pause_pump(obj, **kwargs) -> None:
    """
    Pauses running pump
    """
    send_command(obj["publisher"], "pause_pump", **kwargs)


@click.command()
@click.argument("name")
@click.pass_obj
def unpause_pump(obj, **kwargs) -> None:
    """
    Resumes paused pump
    """
    send_command(obj["publisher"], "unpause_pump", **kwargs)


@click.command()
@click.argument("name")
@click.pass_obj
def stop_pump(obj, **kwargs) -> None:
    """
    Stops pump
    """
    send_command(obj["publisher"], "stop_pump", **kwargs)


@click.command()
@click.argument("name")
@click.pass_obj
def remove_pump(obj, **kwargs) -> None:
    """
    Removes pump
    """
    send_command(obj["publisher"], "remove_pump", **kwargs)


@click.command()
@click.argument("name")
@click.option("-t", "--timeout", type=int, default=1000)
@click.pass_obj
def get_pump(obj, name, timeout) -> None:
    """
    Returns specified pump
    """
    obj["requester"]
    response = send_query(obj["requester"], timeout, query="get_pump", name=name)
    click.echo(response)


@click.command()
@click.option("-t", "--timeout", type=int, default=1000)
@click.pass_obj
def get_pumps(obj, timeout) -> None:
    """
    Returns all active pumps
    """
    response = send_query(obj["requester"], timeout, query="get_pumps")
    click.echo(response)


@click.command()
@click.argument("name")
@click.pass_obj
def set_rate(obj, name) -> None:
    """
    Returns specified pump
    """
    event = {"event_name": "pump_rate_changed", "name": name, "rate": 10}
    obj["publisher"].publish(json.dumps(event))


def add_pump_commands(cli: click.core.Group) -> None:
    cli.add_command(create_pump)
    cli.add_command(start_pump)
    cli.add_command(set_pump_rate)
    cli.add_command(pause_pump)
    cli.add_command(unpause_pump)
    cli.add_command(stop_pump)
    cli.add_command(remove_pump)
    cli.add_command(get_pump)
    cli.add_command(get_pumps)
    cli.add_command(set_rate)
