import ast
import json

import click


def build_command(cmd: str, **kwargs: dict) -> dict:
    return {"action": cmd, **kwargs}


@click.command()
@click.argument("name")
@click.option("-i", "--input-period", type=int, default=1)
@click.pass_obj
def create_monitor(obj, **kwargs) -> None:
    """
    Creates a new monitor instance
    """
    cmd = build_command("create_monitor", **kwargs)
    obj["publisher"].publish(json.dumps(cmd))


@click.command()
@click.argument("name")
@click.option("-o", "--output-period", type=int, default=1)
@click.pass_obj
def change_output_period(obj, **kwargs) -> None:
    """
    Changes the output frequency of the specified monitor
    """
    cmd = build_command("change_output_period", **kwargs)
    obj["publisher"].publish(json.dumps(cmd))


@click.command()
@click.argument("name")
@click.option("-f", "--filters", type=str, default="[]")
@click.pass_obj
def filter_output(obj, name, filters) -> None:
    """
    Filters monitor output to include only provided values
    """
    cmd = build_command("filter_output", name=name, filters=ast.literal_eval(filters))
    obj["publisher"].publish(json.dumps(cmd))


@click.command()
@click.argument("name")
@click.pass_obj
def display_output(obj, name) -> None:
    """
    Display output from specified monitor
    """
    while True:
        output = obj["listener"].receive()
        if output and json.loads(output).get("name") == name:
            click.echo(output)


def add_monitor_commands(cli: click.core.Group) -> None:
    cli.add_command(create_monitor)
    cli.add_command(change_output_period)
    cli.add_command(filter_output)
    cli.add_command(display_output)
