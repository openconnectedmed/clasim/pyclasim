import logging

import click
from algorithms import AlgorithmFactory
from infrastructure import SimulatedMonitorApi, SimulatedPumpApi
from messaging import ZMQMessagingFactory
from patient_conditions import get_patient_conditions

from cli.commands.monitor import add_monitor_commands
from cli.commands.pump import add_pump_commands
from main import create_simulation, get_settings

logging.basicConfig(
    level=logging.INFO,
    format="[%(asctime)s] %(name)s %(levelname)s %(message)s",
)


@click.group()
@click.pass_context
def cli(ctx) -> None:
    settings = get_settings()["messaging"]["frontend"]["zmq"]

    ctx.obj["publisher"] = ZMQMessagingFactory.create_publisher(settings["publisher"])
    ctx.obj["listener"] = ZMQMessagingFactory.create_listener(settings["listener"])
    ctx.obj["requester"] = ZMQMessagingFactory.create_requester(settings["requester"])


@cli.command()
@click.option(
    "--reset-results-dir/--no-reset-results-dir",
    default=True,
    help="Reset existing results directory if True",
)
def run_simulation(reset_results_dir: bool) -> None:
    simulation = create_simulation(patient_conditions=get_patient_conditions())
    simulation.run(reset_results_dir=reset_results_dir)


@cli.command()
@click.option(
    "-n",
    "--name",
    required=True,
    default="hypotension",
    type=str,
    help="Name of Management Algorithm to run",
)
@click.option(
    "-p",
    "--pump_id",
    required=True,
    type=str,
    help="ID of pump that the algorithm will control",
)
@click.option(
    "-m",
    "--monitor_id",
    required=True,
    type=str,
    help="ID of monitor from which to receive patient data",
)
@click.pass_obj
def run_algorithm(obj, name, pump_id, monitor_id) -> None:
    settings = get_settings(config_file="config.algorithms.yaml")

    pump = SimulatedPumpApi(pump_id, obj["publisher"], obj["requester"])
    monitor = SimulatedMonitorApi(monitor_id, obj["listener"])

    algorithm = AlgorithmFactory.create_algorithm(name, pump, monitor, settings)
    algorithm.run()


add_monitor_commands(cli)
add_pump_commands(cli)


if __name__ == "__main__":
    cli(obj={})
