from setuptools import find_packages, setup

setup(
    name="foundation",
    version="0.0.1",
    packages=find_packages(include=["foundation", "foundation.*"]),
    extras_requires={"dev": ["pytest"]},
    python_requires=">=3.8",
)
