import logging
from dataclasses import dataclass
from typing import Callable, Dict, List, Type

logger = logging.getLogger(__name__)


class Event:
    pass


@dataclass
class EventBus:
    handlers: Dict[Type[Event], List[Callable[[Event], None]]]

    def publish(self, events: List[Event]) -> None:
        for event in events:
            self._publish_event(event)

    def _publish_event(self, event: Event) -> None:
        logger.debug(f"Handling: '{event.__class__.__name__}'")
        try:
            event_handlers = self.handlers[type(event)]
            for handler in event_handlers:
                handler(event)

        except Exception:
            logger.error(
                f"Failed to handle: '{event.__class__.__name__}'", exc_info=True
            )
