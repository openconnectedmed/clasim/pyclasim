import logging
from dataclasses import dataclass
from typing import Any, Callable, Dict, Optional, Type, Union

logger = logging.getLogger(__name__)


class Command:
    pass


class Query:
    pass


Message = Union[Command, Query]


@dataclass
class MessageBus:
    handlers: Dict[Type[Message], Callable[[Message], Any]]

    def dispatch(self, msg: Message) -> Optional[Dict[str, Any]]:
        logger.debug(f"Handling: '{msg.__class__.__name__}'")
        try:
            handler = self.handlers[type(msg)]
            return handler(msg)

        except Exception:
            logger.error(f"Failed to handle: '{msg.__class__.__name__}'", exc_info=True)
