import csv
import io
import os
from dataclasses import dataclass
from datetime import datetime
from typing import Any, Dict


@dataclass
class CSVLogger:
    base_dir: str
    filename: str
    writer: csv.DictWriter = None

    def __post_init__(self) -> None:
        file_path = os.path.join(self.base_dir, self.filename)
        self.csv_file = io.open(file_path, mode="a+")

    def record_data_to_file(self, data: Dict[str, Any], header: bool = True) -> None:
        if not self.writer:
            self._initialize_writer(data, header=header)
        self._writerow(data)

    def _initialize_writer(self, data: Dict[str, Any], header: bool) -> None:
        fieldnames = ["system_clock"] + list(data.keys())
        self.writer = csv.DictWriter(self.csv_file, fieldnames=fieldnames)
        if header:
            self.writer.writeheader()

    def _writerow(self, data: Dict[str, Any]) -> None:
        now = datetime.now()
        self.writer.writerow({"system_clock": now.strftime("%X.%f")[:-3], **data})
        self.csv_file.flush()

    def __del__(self) -> None:
        if self.csv_file:
            self.csv_file.close()
