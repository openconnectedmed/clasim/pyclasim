from typing import Callable, Dict, Type, Union

from injector import Binder, Module, multiprovider, provider, singleton

from foundation.messages import Command, Query

from monitor.application.commands import (
    ChangeMonitorOutputPeriodCommand,
    ChangeMonitorOutputPeriodHandler,
    CreateMonitorCommand,
    CreateMonitorHandler,
    FilterMonitorOutputCommand,
    FilterMonitorOutputHandler,
)
from monitor.application.config import MonitorConfig
from monitor.application.repositories import MonitorsRepository
from monitor.application.use_cases import (
    PatientPort,
    UpdatingMonitorsOutputBoundary,
    UpdatingMonitorsOutputDto,
    UpdatingMonitorsUseCase,
)
from monitor.infrastructure.adapters import PulsePatientAdapter
from monitor.infrastructure.repositories import InMemoryMonitorsRepo
from patient_models.pulse import PulseFacade

__all__ = [
    "MonitorConfig",
    "UpdatingMonitorsOutputBoundary",
    "UpdatingMonitorsOutputDto",
    # commands
    "CreateMonitorCommand",
    "FilterMonitorOutputCommand",
    "ChangeMonitorOutputPeriodCommand",
]


class MonitorModule(Module):
    @provider
    def updating_monitors_uc(
        self,
        repo: MonitorsRepository,
        patient: PatientPort,
        presenter: UpdatingMonitorsOutputBoundary,
    ) -> UpdatingMonitorsUseCase:
        return UpdatingMonitorsUseCase(repo, patient, presenter)

    @multiprovider
    def provide_monitor_handlers(
        self, repo: MonitorsRepository, config: MonitorConfig
    ) -> Dict[Union[Type[Command], Type[Query]], Callable]:
        return {
            CreateMonitorCommand: CreateMonitorHandler(repo, config),
            ChangeMonitorOutputPeriodCommand: ChangeMonitorOutputPeriodHandler(repo),
            FilterMonitorOutputCommand: FilterMonitorOutputHandler(repo),
        }


class MonitorInfrastructureModule(Module):
    def configure(self, binder: Binder) -> None:
        binder.bind(MonitorsRepository, to=InMemoryMonitorsRepo(), scope=singleton)

    @provider
    def provide_patient_adapter(self, facade: PulseFacade) -> PatientPort:
        return PulsePatientAdapter(facade)
