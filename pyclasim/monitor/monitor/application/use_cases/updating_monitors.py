import abc
from dataclasses import dataclass
from typing import Dict, List

from monitor.application.repositories import MonitorsRepository


@dataclass(frozen=True)
class UpdatingMonitorsOutputDto:
    name: str
    output: Dict[str, float]


class UpdatingMonitorsOutputBoundary(abc.ABC):
    @abc.abstractmethod
    def present(self, dto: UpdatingMonitorsOutputDto) -> None:
        pass


class PatientPort(abc.ABC):
    @abc.abstractmethod
    def get_vitals_data(self) -> Dict[str, List[float]]:
        pass


@dataclass
class UpdatingMonitorsUseCase:
    repo: MonitorsRepository
    patient: PatientPort
    output_boundary: UpdatingMonitorsOutputBoundary

    def execute(self, timestep: int) -> None:
        patient_data = self.patient.get_vitals_data()

        for monitor in self.repo.get_all_monitors():
            if output_data := monitor.update(timestep, patient_data):
                output_dto = UpdatingMonitorsOutputDto(
                    name=monitor.name,
                    output=output_data,
                )
                self.output_boundary.present(output_dto)
