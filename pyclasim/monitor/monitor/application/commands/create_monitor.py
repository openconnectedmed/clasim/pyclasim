import logging
from dataclasses import dataclass
from typing import Optional

from foundation.csv_logger import CSVLogger

from monitor.application.commands.base import CommandHandler, MonitorCommand
from monitor.application.config import MonitorConfig
from monitor.application.exceptions import MonitorNotFoundError
from monitor.domain.entities import Monitor
from monitor.domain.value_objects import Period

logger = logging.getLogger("monitor.application.commands")


@dataclass(frozen=True)
class CreateMonitorCommand(MonitorCommand):
    name: str
    input_period: Optional[int] = 1


@dataclass
class CreateMonitorHandler(CommandHandler):
    config: MonitorConfig

    def __call__(self, cmd: MonitorCommand) -> None:
        try:
            self.repo.get_monitor_by_name(cmd.name)

        except MonitorNotFoundError:
            monitor = Monitor(
                name=cmd.name,
                input_period=Period(cmd.input_period),
                csv_logger=CSVLogger(self.config.results_dir, f"{cmd.name}.csv"),
                waveform_requests=self.config.waveform_requests,
            )
            self.repo.save(monitor)
            logger.info(f"'{cmd.name}' has been created")

        else:
            logger.error(f"Can't create '{cmd.name}', it already exits")
