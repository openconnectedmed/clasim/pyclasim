from dataclasses import dataclass

from monitor.application.commands.base import CommandHandler, MonitorCommand
from monitor.application.exceptions import MonitorNotFoundError
from monitor.domain.value_objects import Period


@dataclass(frozen=True)
class ChangeMonitorOutputPeriodCommand(MonitorCommand):
    name: str
    output_period: int


class ChangeMonitorOutputPeriodHandler(CommandHandler):
    def __call__(self, cmd: MonitorCommand) -> None:
        try:
            monitor = self.repo.get_monitor_by_name(cmd.name)
            monitor.output_period = Period(cmd.output_period)
            self.repo.save(monitor)

        except MonitorNotFoundError:
            raise
