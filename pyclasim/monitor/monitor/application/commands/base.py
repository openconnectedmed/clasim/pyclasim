from dataclasses import dataclass

from foundation.messages import Command

from monitor.application.repositories import MonitorsRepository


class MonitorCommand(Command):
    pass


@dataclass
class CommandHandler:
    repo: MonitorsRepository
