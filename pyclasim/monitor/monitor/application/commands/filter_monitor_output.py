from dataclasses import dataclass
from typing import List

from monitor.application.commands.base import CommandHandler, MonitorCommand
from monitor.application.exceptions import MonitorNotFoundError


@dataclass(frozen=True)
class FilterMonitorOutputCommand(MonitorCommand):
    name: str
    filters: List[str]


class FilterMonitorOutputHandler(CommandHandler):
    def __call__(self, cmd: MonitorCommand) -> None:
        try:
            monitor = self.repo.get_monitor_by_name(cmd.name)
            monitor.filters = cmd.filters
            self.repo.save(monitor)

        except MonitorNotFoundError:
            raise
