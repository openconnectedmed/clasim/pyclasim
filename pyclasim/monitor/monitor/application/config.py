import os
from dataclasses import dataclass
from typing import List


@dataclass(frozen=True)
class MonitorConfig:
    # list representing path to where results will
    # be stored. ie ["results", "dir"] corresponds to /results/dir
    _results_path: List[str]

    waveform_requests: List[str]

    @property
    def results_dir(self) -> str:
        return os.path.expanduser(os.path.sep.join(self._results_path))
