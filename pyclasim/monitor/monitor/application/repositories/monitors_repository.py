import abc
from typing import List

from monitor.domain.entities import Monitor


class MonitorsRepository(abc.ABC):
    @abc.abstractmethod
    def get_monitor_by_name(self, name: str) -> Monitor:
        pass

    @abc.abstractmethod
    def save(self, entity: Monitor) -> None:
        pass

    @abc.abstractmethod
    def get_all_monitors(self) -> List[Monitor]:
        pass
