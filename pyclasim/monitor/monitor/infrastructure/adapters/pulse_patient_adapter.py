from dataclasses import dataclass
from typing import Dict, List

from monitor.application.use_cases import PatientPort
from patient_models.pulse import PulseFacade


@dataclass
class PulsePatientAdapter(PatientPort):
    facade: PulseFacade

    def get_vitals_data(self) -> Dict[str, List[float]]:
        vitals_data = self.facade.get_vitals_data()
        vitals_data = {
            self._format_name(name): value for name, value in vitals_data.items()
        }
        return vitals_data

    def _format_name(self, name: str) -> str:
        name = self._remove_hyphen(name)
        return self._remove_unit(name)

    def _remove_hyphen(self, name: str) -> str:
        if "-" in name:
            return name.replace(" - ", "")
        return name

    def _remove_unit(self, name: str) -> str:
        return name[: name.find("(")].strip()
