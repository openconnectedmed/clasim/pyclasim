from dataclasses import dataclass, field
from typing import Dict, List

from monitor.application.exceptions import MonitorNotFoundError
from monitor.application.repositories import MonitorsRepository
from monitor.domain.entities import Monitor


@dataclass(repr=False)
class InMemoryMonitorsRepo(MonitorsRepository):
    monitors: Dict[str, Monitor] = field(init=False, default_factory=dict)

    def get_monitor_by_name(self, name: str) -> Monitor:
        if not self.monitors.get(name):
            raise MonitorNotFoundError(name)

        return self.monitors.get(name)

    def get_all_monitors(self) -> List[Monitor]:
        return list(self.monitors.values())

    def save(self, monitor: Monitor) -> None:
        self.monitors[monitor.name] = monitor
