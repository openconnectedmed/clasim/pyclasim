import logging
from typing import Dict, List, Optional, Union

from foundation.csv_logger import CSVLogger

from monitor.domain.value_objects import Period

logger = logging.getLogger(__name__)


class Monitor:
    def __init__(
        self,
        name,
        csv_logger: CSVLogger,
        waveform_requests: List[str] = [],
        input_period: Optional[Period] = Period(1),
    ) -> None:
        self._name = name
        self._csv_logger = csv_logger
        self._waveform_requests = waveform_requests
        self._input_period = input_period
        self._output_period = input_period
        self._filters: List[str] = []

    @property
    def name(self) -> str:
        return self._name

    @property
    def output_period(self) -> Period:
        return self._output_period

    @output_period.setter
    def output_period(self, period: Period) -> None:
        if period < self._input_period:
            raise ValueError(
                "output period cannot be less than input period, "
                f"output_period: {period} input_period: {self._input_period}"
            )

        if not period % self._input_period == 0:
            raise ValueError(
                "output period and input period are expected to be multiples, "
                f"output_period: {period} input_period: {self._input_period}"
            )

        if period == self._output_period:
            return

        self._output_period = period
        logger.info(f"'{self._name}' output period set to {period.value}")

    @property
    def filters(self) -> List[str]:
        return self._filters

    @filters.setter
    def filters(self, filters: List[str]) -> None:
        if not isinstance(filters, list):
            raise TypeError(
                "expects filters to be a list not " f"{filters.__class__.__name__}"
            )
        if sorted(self._filters) != sorted(filters):
            self._filters = filters
            logger.info(
                f"'{self._name}' set to output "
                f"{self._filters if self._filters else 'all variables'}"
            )

    def update(
        self, timestep: int, input_data: Dict[str, List[float]]
    ) -> Optional[Dict[str, Union[float, List[float]]]]:
        if not (data := self._input_update(timestep, input_data)):
            return

        return self._output_update(timestep, data)

    def _input_update(
        self, timestep: int, input_data: Dict[str, List[float]]
    ) -> Optional[Dict[str, List[float]]]:
        if not timestep % self._input_period.value == 0:
            return

        self._record_data(data=input_data, name="input")
        return input_data

    def _output_update(
        self, timestep: int, data: Dict[str, List[float]]
    ) -> Optional[Dict[str, Union[float, List[float]]]]:
        if not timestep % self._output_period.value == 0:
            return

        if self.filters:
            data = self._filter_data(data)

        self._record_data(data=data, name="output")
        return self._preprocess_for_output(data)

    def _record_data(self, data: Dict[str, List[float]], name: str) -> None:
        self._csv_logger.record_data_to_file(
            {
                **self._preprocess_for_file(data),
                "update_step_name": name,
            }
        )

    def _filter_data(self, data: Dict[str, List[float]]) -> Dict[str, List[float]]:
        if not self._validate_filters(self._filters, vital_signs=list(data.keys())):
            logger.error(
                f"provided filters for '{self._name}' contain invalid values, "
                f"{self._filters}"
            )
            self._reset_filters()
            return data

        return self._get_filtered_data(self._filters, data)

    @staticmethod
    def _validate_filters(filters: List[str], vital_signs: List[str]) -> bool:
        for f in filters:
            if f not in vital_signs:
                return False

        return True

    @staticmethod
    def _get_filtered_data(
        filters: List[str], data: Dict[str, List[float]]
    ) -> Dict[str, List[float]]:
        filters = filters + ["SimulationTime"]
        return {k: v for k, v in data.items() if k in filters}

    def _reset_filters(self) -> None:
        logger.info("resetting filters")
        self._filters = []

    def _preprocess_for_output(
        self, data: Dict[str, List[float]]
    ) -> Dict[str, Union[float, List[float]]]:
        output_data = {}

        for vital_sign, values in data.items():
            if vital_sign in self._waveform_requests:
                output_data[vital_sign] = {
                    "times": data["SimulationTime"],
                    "values": values,
                }

            else:
                output_data[vital_sign] = values[-1]

        return output_data

    def _preprocess_for_file(self, data: Dict[str, List[float]]) -> Dict[str, float]:
        file_data = {
            vital_sign: values[-1]
            for vital_sign, values in data.items()
            if vital_sign not in self._waveform_requests
        }

        return file_data

    def __eq__(self, other: "Monitor") -> bool:
        if not isinstance(other, Monitor):
            return False
        return self._name == other.name
