from dataclasses import dataclass


@dataclass(order=True, frozen=True)
class Period:
    value: int

    def __post_init__(self):
        if not isinstance(self.value, int):
            raise TypeError(f"Expects an 'int' not {self.value.__class__.__name__}")

        if self.value <= 0:
            raise ValueError("Period value cannot be zero or negative")

    def __mod__(self, other: "Period") -> int:
        if not isinstance(other, Period):
            TypeError
        return self.value % other.value
