from unittest.mock import Mock

import pytest

from monitor.application.exceptions import MonitorNotFoundError
from monitor.domain.entities import Monitor
from monitor.infrastructure.repositories import InMemoryMonitorsRepo


@pytest.fixture()
def monitors_repo():
    return InMemoryMonitorsRepo()


def test_raises_exception_if_monitor_not_found(monitors_repo):

    with pytest.raises(MonitorNotFoundError):
        monitors_repo.get_monitor_by_name("monitor")


def test_returns_stored_monitor(monitors_repo):
    monitor = Monitor(name="monitor", csv_logger=Mock())
    monitors_repo.save(monitor)

    saved_monitor = monitors_repo.get_monitor_by_name("monitor")

    assert saved_monitor == monitor


def test_gets_all_stored_monitors(monitors_repo):
    monitor1 = Monitor(name="monitor1", csv_logger=Mock())
    monitor2 = Monitor(name="monitor2", csv_logger=Mock())
    monitors_repo.save(monitor1)
    monitors_repo.save(monitor2)

    monitors = monitors_repo.get_all_monitors()

    assert len(monitors) == 2
    assert monitor1 in monitors
    assert monitor2 in monitors
