import tempfile
from typing import List
from unittest.mock import Mock

import pytest

import monitor.application.commands as cmds
from monitor.application.exceptions import MonitorNotFoundError
from monitor.domain.entities import Monitor
from monitor.domain.value_objects import Period


class FakeMonitorConfig:
    @property
    def waveform_requests(self) -> List[str]:
        return Mock()

    @property
    def results_dir(self) -> str:
        return tempfile.mkdtemp()


@pytest.fixture()
def mock_monitors_repo():
    mock_repo = Mock()
    test_monitor = Monitor("monitor", csv_logger=Mock())
    mock_repo.get_monitor_by_name.return_value = test_monitor
    return mock_repo


def test_handler_creates_new_monitor_and_saves_it_to_repo_if_it_doesnot_exist_already(
    mock_monitors_repo,
):
    mock_monitors_repo.get_monitor_by_name.side_effect = MonitorNotFoundError("")
    create_cmd = cmds.CreateMonitorCommand("new_monitor")

    handler = cmds.CreateMonitorHandler(mock_monitors_repo, config=FakeMonitorConfig())
    handler(create_cmd)

    mock_monitors_repo.save.assert_called_once()


def test_handler_cannot_create_monitor_if_it_already_exists(mock_monitors_repo):
    create_cmd = cmds.CreateMonitorCommand("monitor")

    handler = cmds.CreateMonitorHandler(mock_monitors_repo, config=FakeMonitorConfig())
    handler(create_cmd)

    mock_monitors_repo.save.not_assert_called()


def test_handler_sets_filters(mock_monitors_repo):
    filters = ["heart_rate", "blood_pressure"]
    filter_cmd = cmds.FilterMonitorOutputCommand("monitor", filters=filters)

    handler = cmds.FilterMonitorOutputHandler(mock_monitors_repo)
    handler(filter_cmd)

    monitor = mock_monitors_repo.get_monitor_by_name("monitor")
    assert monitor.filters == filters


def test_handler_changes_output_period(mock_monitors_repo):
    change_period_cmd = cmds.ChangeMonitorOutputPeriodCommand("monitor", 5)

    handler = cmds.ChangeMonitorOutputPeriodHandler(mock_monitors_repo)
    handler(change_period_cmd)

    monitor = mock_monitors_repo.get_monitor_by_name("monitor")
    assert monitor.output_period == Period(5)
