from unittest.mock import Mock

import pytest

from monitor.domain.entities import Monitor
from monitor.domain.value_objects import Period


@pytest.fixture()
def input_data():
    data = {
        "SimulationTime": [10.02, 10.04],
        "HeartRate": [60, 65],
        "MeanArterialPressure": [70, 72],
        "OxygenSaturation": [95, 99],
        "SystolicArterialPressure": [70, 90],
        "DiastolicArterialPressure": [110, 120],
        "ArterialPressure": [80, 85],
        "RespirationRate": [15, 16],
        "EndTidalCarbonDioxidePressure": [10, 11],
        "SkinTemperature": [34, 35],
        "BloodVolume": [5000, 5000],
        "CardiacOutput": [5, 5],
        "Lead3ElectricPotential": [0.05, 0.4],
    }
    return data


@pytest.fixture()
def expected_output_data():
    output_data = {
        "SimulationTime": 10.04,
        "HeartRate": 65,
        "MeanArterialPressure": 72,
        "OxygenSaturation": 99,
        "SystolicArterialPressure": 90,
        "DiastolicArterialPressure": 120,
        "ArterialPressure": {"times": [10.02, 10.04], "values": [80, 85]},
        "RespirationRate": 16,
        "EndTidalCarbonDioxidePressure": 11,
        "SkinTemperature": 35,
        "BloodVolume": 5000,
        "CardiacOutput": 5,
        "Lead3ElectricPotential": {"times": [10.02, 10.04], "values": [0.05, 0.4]},
    }
    return output_data


@pytest.fixture()
def monitor():
    return Monitor(
        name="monitor1",
        input_period=Period(2),
        csv_logger=Mock(),
        waveform_requests=["Lead3ElectricPotential", "ArterialPressure"],
    )


@pytest.mark.parametrize("value", [(1.5), ("")])
def test_creation_raises_exception_if_period_is_not_int(monitor, value):
    with pytest.raises(TypeError):
        Monitor(name="monitor", input_period=Period(value), csv_logger=Mock())


@pytest.mark.parametrize("value", [(0), (-1), (-20)])
def test_creation_raises_exception_if_period__le__zero(monitor, value):
    with pytest.raises(ValueError):
        Monitor(name="monitor", input_period=Period(value), csv_logger=Mock())


def test_output_period__eq__input_period_after_creation(monitor):
    monitor.output_period = Period(2)


@pytest.mark.parametrize("output_period", [(3), (5)])
def test_raises_exception_if_set_output_period_is_not_multiple_of_input_period(
    monitor, output_period
):
    with pytest.raises(ValueError):
        monitor.output_period = Period(output_period)


def test_raises_exception_if_set_outputperiod_value__lt__inputperiod(monitor):
    with pytest.raises(ValueError):
        monitor.output_period = Period(1)


@pytest.mark.parametrize("filters", [("string"), (1), ({})])
def test_raise_exception_if_filters_type_is_not_list(monitor, filters):
    with pytest.raises(TypeError):
        monitor.filters = filters


def test_update_resets_filters_when_invalid_filters_are_provided(
    monitor, input_data, expected_output_data
):
    monitor.filters = ["random_string"]
    output = monitor.update(timestep=0, input_data=input_data)

    assert output == expected_output_data


@pytest.mark.parametrize(
    "filters, expected",
    [
        (
            ["HeartRate", "MeanArterialPressure"],
            {
                "SimulationTime": 10.04,
                "HeartRate": 65,
                "MeanArterialPressure": 72,
            },
        ),
        (
            ["SystolicArterialPressure"],
            {"SimulationTime": 10.04, "SystolicArterialPressure": 90},
        ),
    ],
)
def test_update_outputs_filtered_data_when_valid_filters_are_provided(
    monitor, input_data, filters, expected
):
    monitor.filters = filters
    output = monitor.update(timestep=0, input_data=input_data)

    assert output == expected


def test_update_resets_filters_when_empty_list_is_provided(
    monitor, input_data, expected_output_data
):
    monitor.filters = []
    output = monitor.update(timestep=0, input_data=input_data)

    assert output == expected_output_data


@pytest.mark.parametrize("timestep", [(1), (3), (5)])
def test_update_outputs_no_data_if_timestep_doesnot_match_outputperiod(
    monitor, input_data, timestep
):
    monitor.output_period = Period(2)
    output = monitor.update(timestep, input_data)

    assert output is None


@pytest.mark.parametrize("timestep", [(0), (2), (4)])
def test_update_outputs_data_if_timestep_matches_outputperiod(
    monitor, input_data, timestep, expected_output_data
):
    monitor.output_period = Period(2)
    output = monitor.update(timestep, input_data)

    assert output == expected_output_data
