from dataclasses import dataclass
from typing import Dict, List, Union
from unittest.mock import Mock, call

import pytest

from monitor.application.use_cases import (
    UpdatingMonitorsOutputDto,
    UpdatingMonitorsUseCase,
)

output_test_data = {
    "SimulationTime": 10.04,
    "HeartRate": 65,
    "MeanArterialPressure": 72,
    "OxygenSaturation": 99,
}


@dataclass
class FakeMonitor:
    name: str

    def update(
        self, timestep: float, vitals_data: Dict[str, List[float]]
    ) -> Dict[str, Union[float, List[float]]]:
        return output_test_data


@pytest.fixture()
def mock_monitors_repo():
    mock_repo = Mock()
    mock_repo.get_all_monitors.return_value = [
        FakeMonitor("monitor1"),
        FakeMonitor("monitor2"),
    ]
    return mock_repo


@pytest.fixture()
def mock_presenter():
    return Mock()


@pytest.fixture()
def mock_patient_model():
    patient = Mock()
    patient.get_vitals_data.return_value = {
        "SimulationTime": [10.02, 10.04],
        "HeartRate": [60, 65],
        "MeanArterialPressure": [70, 72],
        "OxygenSaturation": [95, 99],
    }
    return patient


def test_calls_presenter_with_monitor_output_data_on_execution(
    mock_monitors_repo, mock_presenter, mock_patient_model
):
    updating_monitors_uc = UpdatingMonitorsUseCase(
        mock_monitors_repo, mock_patient_model, mock_presenter
    )
    updating_monitors_uc.execute(timestep=0)

    assert mock_presenter.present.call_count == 2
    mock_presenter.present.assert_has_calls(
        [
            call(UpdatingMonitorsOutputDto("monitor1", output_test_data)),
            call(UpdatingMonitorsOutputDto("monitor2", output_test_data)),
        ]
    )
