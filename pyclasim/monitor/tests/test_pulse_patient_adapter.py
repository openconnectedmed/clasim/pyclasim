from unittest.mock import Mock

import pytest

from monitor.infrastructure.adapters import PulsePatientAdapter


@pytest.fixture()
def mock_pulse_facade():
    facade = Mock()
    facade.get_vitals_data.return_value = {
        "SimulationTime(s)": [10.02, 10.04],
        "HeartRate (1/min)": [60, 65],
        "MeanArterialPressure (mmHg)": [70, 72],
        "OxygenSaturation (None)": [95, 99],
        "SystolicArterialPressure (mmHg)": [70, 90],
        "DiastolicArterialPressure (mmHg)": [110, 120],
        "ArterialPressure (mmHg)": [80, 85],
        "RespirationRate (1/min)": [15, 16],
        "EndTidalCarbonDioxidePressure (mmHg)": [10, 11],
        "SkinTemperature (degC)": [34, 35],
        "BloodVolume (mL)": [5000, 5000],
        "CardiacOutput (L/min)": [5, 5],
        "Lead3ElectricPotential (mV)": [0.05, 0.4],
        "Carina - CarbonDioxide - PartialPressure (mmHg)": [34.7, 20.2],
    }

    return facade


def test_returns_formatted_key_for_vitals_data(mock_pulse_facade):
    patient = PulsePatientAdapter(mock_pulse_facade)

    vitals_data = patient.get_vitals_data()

    expected_data = {
        "SimulationTime": [10.02, 10.04],
        "HeartRate": [60, 65],
        "MeanArterialPressure": [70, 72],
        "OxygenSaturation": [95, 99],
        "SystolicArterialPressure": [70, 90],
        "DiastolicArterialPressure": [110, 120],
        "ArterialPressure": [80, 85],
        "RespirationRate": [15, 16],
        "EndTidalCarbonDioxidePressure": [10, 11],
        "SkinTemperature": [34, 35],
        "BloodVolume": [5000, 5000],
        "CardiacOutput": [5, 5],
        "Lead3ElectricPotential": [0.05, 0.4],
        "CarinaCarbonDioxidePartialPressure": [34.7, 20.2],
    }

    assert vitals_data == expected_data
