from setuptools import find_packages, setup

setup(
    name="monitor",
    version="0.0.1",
    packages=find_packages(include=["monitor", "monitor.*"]),
    install_requires=[
        "injector",
        "foundation",
        "patient_models",
    ],
    extras_requires={"dev": ["pytest"]},
    python_requires=">=3.8",
)
