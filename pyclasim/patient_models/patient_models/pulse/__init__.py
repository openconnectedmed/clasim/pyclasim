from typing import Callable, List, Tuple, Type

from injector import Module, multiprovider, provider, singleton

from foundation.events import Event

from patient_models.pulse.config import PulseConfig
from patient_models.pulse.facade import PulseFacade
from patient_models.pulse.handlers import (
    InfusionsRepo,
    PumpConnectedHandler,
    PumpRateChangedHandler,
)
from pump import PumpConnected, PumpPaused, PumpRateChanged, PumpStopped, PumpUnpaused

__all__ = [
    "PulseConfig",
    "PulseFacade",
    "PulseModule",
    # event handlers
    "PumpConnectedHandler",
    "PumpRateChangedHandler",
]


class PulseModule(Module):
    @singleton
    @provider
    def provide_infusions_repo(self) -> InfusionsRepo:
        return InfusionsRepo()

    @provider
    def provide_pulse_facade(self, config: PulseConfig) -> PulseFacade:
        return PulseFacade(config)

    @multiprovider
    def event_handlers(
        self, facade: PulseFacade, config: PulseConfig, repo: InfusionsRepo
    ) -> List[Tuple[Type[Event], Callable[[Event], None]]]:
        return [
            (PumpConnected, PumpConnectedHandler(config, repo)),
            (PumpPaused, PumpRateChangedHandler(facade, repo)),
            (PumpRateChanged, PumpRateChangedHandler(facade, repo)),
            (PumpStopped, PumpRateChangedHandler(facade, repo)),
            (PumpUnpaused, PumpRateChangedHandler(facade, repo)),
        ]
