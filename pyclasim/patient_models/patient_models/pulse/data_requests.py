import logging
import os
from typing import List

from pulse.cdm.engine import SEDataRequest, SEDataRequestManager

from patient_models.pulse.config import PulseConfig

logger = logging.getLogger(__name__)


def _prepare_data_requests(config: PulseConfig) -> List[SEDataRequest]:
    physiological_data_requests = [
        SEDataRequest.create_physiology_request(physiology, unit=unit)
        for (physiology, unit) in config.data_requests["physiological_data"]
    ]

    ecg_data_request = [
        SEDataRequest.create_ecg_request(ecg, unit=unit)
        for (ecg, unit) in config.data_requests["ecg_data"]
    ]

    gas_compartment_substance_requests = [
        SEDataRequest.create_gas_compartment_substance_request(
            compartment, substance, _property, unit
        )
        for (compartment, substance, _property, unit) in config.data_requests[
            "gas_compartment_substance"
        ]
    ]

    data_requests = (
        physiological_data_requests
        + ecg_data_request
        + gas_compartment_substance_requests
    )

    return data_requests


def initialize_data_request_manager(
    config: PulseConfig,
) -> SEDataRequestManager:
    data_requests = _prepare_data_requests(config)
    data_req_mgr = SEDataRequestManager(data_requests)

    file_path = os.path.join(config.results_dir, config.data_requests_file)
    data_req_mgr.set_results_filename(file_path)

    return data_req_mgr
