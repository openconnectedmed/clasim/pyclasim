import os
from dataclasses import dataclass
from typing import Dict, List, Optional

from pulse.cdm.engine import SEAction
from pulse.cpm.PulsePhysiologyEngine import PulsePhysiologyEngine

from patient_models.base import PatientCondition
from patient_models.pulse.config import PulseConfig
from patient_models.pulse.exceptions import MissingEnvironmentVariable
from patient_models.pulse.prepare_engine import prepare_engine


@dataclass
class PulseFacade:
    try:
        os.environ["PULSE_DATA_ROOT_DIR"]
    except KeyError as e:
        raise MissingEnvironmentVariable(e) from None

    config: PulseConfig
    pulse: PulsePhysiologyEngine = PulsePhysiologyEngine()
    patient_conditions: Optional[List[PatientCondition]] = None

    def initialize_patient(
        self, patient_conditions: Optional[List[PatientCondition]] = None
    ) -> None:
        file_path = os.path.join(self.config.results_dir, self.config.pulse_log_file)
        self.pulse.set_log_filename(file_path)
        self.pulse.log_to_console(True)

        prepare_engine(self.pulse, self.config)
        self._initialize_patient_conditions(patient_conditions)

    def _initialize_patient_conditions(
        self, patient_conditions: Optional[List[PatientCondition]] = None
    ) -> None:
        if patient_conditions is not None:
            self.patient_conditions = patient_conditions
            for condition in self.patient_conditions:
                condition.initialize(self.pulse)

    def get_vitals_data(self) -> Dict[str, List[float]]:
        return self.pulse.pull_data()

    def process_action(self, action: SEAction) -> None:
        self.pulse.process_action(action)

    def advance(self, timestep: int) -> None:
        if self.patient_conditions is not None:
            for condition in self.patient_conditions:
                condition.execute(timestep, self.pulse)

        self.pulse.advance_time_sample_per_s(
            self.config.duration_per_timestep, self.config.sampling_rate
        )
