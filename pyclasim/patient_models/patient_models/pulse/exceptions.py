from typing import Any


class MissingEnvironmentVariable(Exception):
    def __init__(self, variable: Any) -> None:
        self.message = (
            f"Tried accessing an environment variable that doesn't exist, {variable}"
        )
        super().__init__(self.message)
