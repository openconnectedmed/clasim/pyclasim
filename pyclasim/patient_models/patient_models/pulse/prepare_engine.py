import logging
import os
import sys

from pulse.cdm.engine import SEDataRequestManager
from pulse.cdm.io.patient import serialize_patient_from_file
from pulse.cdm.patient import SEPatientConfiguration
from pulse.cdm.scalars import (
    FrequencyUnit,
    LengthUnit,
    MassUnit,
    PressureUnit,
    TimeUnit,
)
from pulse.cpm.PulsePhysiologyEngine import PulsePhysiologyEngine

from patient_models.pulse.config import PulseConfig
from patient_models.pulse.data_requests import initialize_data_request_manager

logger = logging.getLogger(__name__)


def _configure_patient(config: PulseConfig) -> SEPatientConfiguration:
    data_root_dir = os.getenv("PULSE_DATA_ROOT_DIR")
    patients_dir = "patients"

    pc = SEPatientConfiguration()
    pc.set_data_root_dir(data_root_dir)
    patient = pc.get_patient()

    serialize_patient_from_file(
        os.path.join(data_root_dir, patients_dir, f"{config.patient_name}.json"),
        patient,
    )

    if config.apply_configurations:
        patient.get_age().set_value(
            config.patient_configurations["age"],
            TimeUnit.yr,
        )
        patient.get_height().set_value(
            config.patient_configurations["height"],
            LengthUnit.inch,
        )
        patient.get_weight().set_value(
            config.patient_configurations["weight"],
            MassUnit.lb,
        )
        patient.get_heart_rate_baseline().set_value(
            config.patient_configurations["heart_rate_baseline"],
            FrequencyUnit.Per_min,
        )
        patient.get_systolic_arterial_pressure_baseline().set_value(
            config.patient_configurations["systolic_arterial_pressure_baseline"],
            PressureUnit.mmHg,
        )
        patient.get_diastolic_arterial_pressure_baseline().set_value(
            config.patient_configurations["diastolic_arterial_pressure_baseline"],
            PressureUnit.mmHg,
        )
        patient.get_respiration_rate_baseline().set_value(
            config.patient_configurations["respiration_rate_baseline"],
            FrequencyUnit.Per_min,
        )

    return pc


def _initialize_engine(
    pulse: PulsePhysiologyEngine,
    pc: SEPatientConfiguration,
    data_req_mgr: SEDataRequestManager,
) -> None:
    logger.info("Stabilizing engine, this will take a few minutes...")

    if not pulse.initialize_engine(pc, data_req_mgr):
        logger.error("Unable to load stabilize engine")
        sys.exit()


def prepare_engine(pulse: PulsePhysiologyEngine, config: PulseConfig) -> None:
    data_req_mgr = initialize_data_request_manager(config)
    pc = _configure_patient(config)
    _initialize_engine(pulse, pc, data_req_mgr)
