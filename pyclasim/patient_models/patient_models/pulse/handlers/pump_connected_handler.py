import logging
import os
from dataclasses import dataclass
from typing import ClassVar, List

from pulse.cdm.patient_actions import (
    SEPatientAction,
    SESubstanceCompoundInfusion,
    SESubstanceInfusion,
)
from pulse.cdm.scalars import MassPerVolumeUnit, VolumeUnit

from foundation.events import Event

from patient_models.pulse.config import PulseConfig
from patient_models.pulse.handlers.infusions_repository import InfusionsRepo

logger = logging.getLogger("patient_models.pulse.handlers")


class FluidNames:
    def __init__(self, flag: str) -> None:
        self.flag = flag

    def __get__(self, obj, type=None) -> List[str]:
        config = getattr(obj, "config")

        if self.flag == "substances":
            return self._list_names(config.substances_dir)

        if self.flag == "compounds":
            return self._list_names(config.compounds_dir)

    def _list_names(self, _dir_name: str) -> List[str]:
        dir_path = os.path.sep.join([os.getenv("PULSE_DATA_ROOT_DIR"), _dir_name])
        return [os.path.splitext(f)[0] for f in os.listdir(dir_path)]


@dataclass
class PumpConnectedHandler:
    _substances: ClassVar[List[str]] = FluidNames("substances")
    _compounds: ClassVar[List[str]] = FluidNames("compounds")

    config: PulseConfig
    repo: InfusionsRepo

    def __call__(self, event: Event) -> None:
        self._prepare_infusion(
            event.name, event.fluid, event.bag_volume, event.concentration
        )

    def _prepare_infusion(
        self, name: str, fluid: str, bag_volume: float, concentration: float
    ) -> None:
        if self._is_substance(fluid):
            self.repo.save(name, self._get_substance_infusion(fluid, concentration))
            logger.info(f"'{name}' ready to deliver substance: '{fluid}'")

        elif self._is_compound(fluid):
            self.repo.save(name, self._get_compound_infusion(fluid, bag_volume))
            logger.info(f"'{name}' ready to deliver substance compound: '{fluid}'")

        else:
            logger.error(f'Unknown fluid: "{fluid}"')

    def _is_substance(self, fluid: str) -> bool:
        return fluid in self._substances

    def _is_compound(self, fluid: str) -> bool:
        return fluid in self._compounds

    @staticmethod
    def _get_substance_infusion(fluid: str, concentration: float) -> SEPatientAction:
        infusion = SESubstanceInfusion()
        infusion.set_substance(fluid)
        infusion.get_concentration().set_value(
            concentration, MassPerVolumeUnit.ug_Per_mL
        )
        return infusion

    @staticmethod
    def _get_compound_infusion(fluid: str, bag_volume: float) -> SEPatientAction:
        infusion = SESubstanceCompoundInfusion()
        infusion.set_compound(fluid)
        infusion.get_bag_volume().set_value(bag_volume, VolumeUnit.mL)
        return infusion
