from dataclasses import dataclass, field
from typing import Dict

from pulse.cdm.patient_actions import SEPatientAction


@dataclass
class InfusionsRepo:
    infusions: Dict[str, SEPatientAction] = field(init=False, default_factory=dict)

    def get_infusion_by_pump_name(self, name: str) -> SEPatientAction:
        return self.infusions[name]

    def save(self, name: str, infusion: SEPatientAction) -> None:
        self.infusions[name] = infusion
