from dataclasses import dataclass

from pulse.cdm.scalars import VolumePerTimeUnit

from foundation.events import Event

from patient_models.pulse.facade import PulseFacade
from patient_models.pulse.handlers.infusions_repository import InfusionsRepo


@dataclass
class PumpRateChangedHandler:
    pulse_facade: PulseFacade
    repo: InfusionsRepo

    def __call__(self, event: Event) -> None:
        infusion = self.repo.get_infusion_by_pump_name(event.name)
        infusion.get_rate().set_value(event.rate, VolumePerTimeUnit.mL_Per_hr)
        self.pulse_facade.process_action(infusion)
