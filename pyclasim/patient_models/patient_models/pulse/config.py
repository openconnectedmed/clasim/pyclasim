import os
from dataclasses import dataclass
from typing import Any, Dict, List, Optional, Union


@dataclass(frozen=True)
class PulseConfig:
    duration_per_timestep: float  # seconds to advance the engine for each timestep
    sampling_rate: float  # rate at which to get data from pulse per timestep

    patient_name: str
    patient_configurations: Dict[str, Union[int, float]]
    apply_configurations: bool

    # data requests
    data_requests: Dict[str, List[str]]

    # list representing path to where results will
    # be stored. ie ["results", "dir"] corresponds to /results/dir
    _results_path: List[str]

    # directories containing compounds and substances
    # relative to the PULSE_DATA_ROOT_DIR ie <path-to>/pulse/bin
    substances_dir: Optional[str] = "substances"
    compounds_dir: Optional[str] = "substances/compounds"

    # file where data request values will be written
    data_requests_file: Optional[str] = "patient.csv"

    # file where pulse log output will be written
    pulse_log_file: Optional[str] = "pulse.log"

    @property
    def results_dir(self) -> str:
        return os.path.expanduser(os.path.sep.join(self._results_path))
