import abc
from typing import Any


class PatientCondition(abc.ABC):
    @abc.abstractmethod
    def initialize(self, **kwargs: Any) -> None:
        pass

    @abc.abstractmethod
    def execute(self, timestep: int, **kwargs: Any) -> None:
        # timestep parameter argument has a one to one mapping to seconds
        # since we are running the simulation in real-time(ie updating every 1s)
        pass
