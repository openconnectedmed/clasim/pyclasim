from patient_models.base import PatientCondition

__all__ = [
    "PatientCondition",
]
