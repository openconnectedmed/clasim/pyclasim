from dataclasses import dataclass
from unittest.mock import Mock

from patient_models.pulse.handlers import PumpRateChangedHandler


@dataclass
class FakeRateChangedEvent:
    name: str
    rate: float


def test_calls_pulse_process_action_on_receiving_event():
    mock_facade = Mock()
    mock_infusions_repo = Mock()
    mock_infusion = Mock()
    mock_infusions_repo.get_infusion_by_pump_name.return_value = mock_infusion

    handler = PumpRateChangedHandler(mock_facade, mock_infusions_repo)
    handler(FakeRateChangedEvent("pump", 100))

    mock_facade.process_action.assert_called_with(mock_infusion)
