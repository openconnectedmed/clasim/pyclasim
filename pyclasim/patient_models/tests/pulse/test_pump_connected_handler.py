import os
from dataclasses import dataclass
from unittest.mock import Mock

import pytest

from patient_models.pulse.handlers import PumpConnectedHandler


@dataclass
class FakePumpConnectedEvent:
    name: str
    fluid: str
    bag_volume: float = None
    concentration: float = None


@dataclass
class FakeConfig:
    substances_dir: str = ""
    compounds_dir: str = ""


@pytest.fixture()
def mock_infusions_repo():
    return Mock()


def test_saves_pump_to_infusions_repo(monkeypatch, mock_infusions_repo):
    handler = PumpConnectedHandler(config=FakeConfig(), repo=mock_infusions_repo)

    monkeypatch.setattr(os, "listdir", Mock(return_value=["Saline"]))
    handler(FakePumpConnectedEvent("pump", "Saline", 50))

    called_pump_name, _ = mock_infusions_repo.save.call_args[0]
    assert called_pump_name == "pump"


def test_fails_to_save_pump_in_infusions_repo_if_invalid_fluid_name_provided(
    monkeypatch,
    mock_infusions_repo,
):
    handler = PumpConnectedHandler(config=FakeConfig(), repo=mock_infusions_repo)

    monkeypatch.setattr(os, "listdir", Mock(return_value=["Saline"]))
    handler(FakePumpConnectedEvent("pump", "invalid_fluid", 50))

    mock_infusions_repo.save.assert_not_called()
