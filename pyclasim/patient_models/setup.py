from setuptools import find_packages, setup

setup(
    name="patient_models",
    version="0.0.1",
    packages=find_packages(include=["patient_models", "patient_models.*"]),
    install_requires=[
        "numpy",
        "six",
        "injector",
        "foundation",
        "pump",
    ],
    extras_requires={"dev": ["pytest"]},
    python_requires=">=3.8",
)
