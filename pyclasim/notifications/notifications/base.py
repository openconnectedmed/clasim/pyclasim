import abc

from foundation.events import Event


class Notifier(abc.ABC):
    @abc.abstractmethod
    def __call__(self, event: Event) -> None:
        pass
