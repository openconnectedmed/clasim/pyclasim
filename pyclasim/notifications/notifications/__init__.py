from typing import Any, Callable, Dict, List, Tuple, Type

from injector import Module, multiprovider, provider, singleton

from foundation.events import Event

from notifications.base import Notifier
from notifications.pump_state_change_notifier import ZMQPumpStateChangeNotifier
from pump.domain.events import PumpPaused, PumpRateChanged, PumpStopped, PumpUnpaused

__all__ = [
    "NotificationsModule",
    "ZMQPumpStateChangeNotifier",
]


class NotificationsModule(Module):
    def __init__(self, settings: Dict[str, Any]) -> None:
        self._settings = settings["notifications"]
        self._messaging_type = settings["messaging"]["type"]

    @singleton
    @provider
    def provide_notifier(self) -> Notifier:
        if self._messaging_type == "zmq":
            return ZMQPumpStateChangeNotifier(
                self._settings[self._messaging_type]["publisher"]
            )

    @multiprovider
    def notifiers(
        self, notifier: Notifier
    ) -> List[Tuple[Type[Event], Callable[[Event], None]]]:
        return [
            (PumpPaused, notifier),
            (PumpRateChanged, notifier),
            (PumpStopped, notifier),
            (PumpUnpaused, notifier),
        ]
