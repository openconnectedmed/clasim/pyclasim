import json
from dataclasses import asdict
from typing import Any, Dict

from messaging import ZMQMessagingFactory

from foundation.events import Event

from notifications.base import Notifier


class ZMQPumpStateChangeNotifier(Notifier):
    def __init__(self, settings: Dict[str, Any]) -> None:
        self.publisher = ZMQMessagingFactory.create_publisher(settings)

    def __call__(self, event: Event) -> None:
        msg = json.dumps(asdict(event))
        self.publisher.publish(msg)
