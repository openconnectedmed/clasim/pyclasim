from setuptools import find_packages, setup

setup(
    name="notifications",
    version="0.0.1",
    packages=find_packages(include=["notifications", "notifications.*"]),
    install_requires=[
        "injector",
        "foundation",
        "messaging",
        "pump",
    ],
    extras_requires={"dev": ["pytest"]},
    python_requires=">=3.8",
)
