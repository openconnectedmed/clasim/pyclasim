from typing import Callable, Dict, Type, Union

from injector import Module, provider

from foundation.messages import Command, MessageBus, Query


class MessageBusModule(Module):
    @provider
    def provide_message_bus(
        self, handlers: Dict[Union[Type[Command], Type[Query]], Callable]
    ) -> MessageBus:
        return MessageBus(handlers=handlers)
