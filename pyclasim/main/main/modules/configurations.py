from typing import Any, Dict

from injector import Module, provider, singleton

from monitor import MonitorConfig
from patient_models.pulse import PulseConfig
from pump import PumpConfig


class Configurations(Module):
    def __init__(self, settings: Dict[str, Any]) -> None:
        self._settings = settings
        self._pulse_settings = self._settings["patient_models"]["pulse"]

    @singleton
    @provider
    def pulse_config(self) -> PulseConfig:
        return PulseConfig(
            duration_per_timestep=self._pulse_settings["duration_per_timestep"],
            sampling_rate=self._pulse_settings["sampling_rate"],
            patient_name=self._pulse_settings["patient"]["name"],
            patient_configurations=self._pulse_settings["patient"]["configurations"],
            apply_configurations=self._pulse_settings["patient"][
                "apply_configurations"
            ],
            data_requests=self._pulse_settings["data_requests"],
            _results_path=self._settings["results_path"],
        )

    @singleton
    @provider
    def pump_config(self) -> PumpConfig:
        return PumpConfig(_results_path=self._settings["results_path"])

    @singleton
    @provider
    def monitor_config(self) -> MonitorConfig:
        return MonitorConfig(
            _results_path=self._settings["results_path"],
            waveform_requests=self._settings["devices"]["monitor"]["waveform_requests"],
        )
