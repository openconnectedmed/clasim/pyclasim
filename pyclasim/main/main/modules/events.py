from collections import defaultdict
from typing import Callable, Dict, List, Tuple, Type

from injector import Module, multiprovider, provider, singleton

from foundation.events import Event, EventBus


class EventBusModule(Module):
    @multiprovider
    def provide_handlers(
        self, handlers: List[Tuple[Type[Event], Callable[[Event], None]]]
    ) -> Dict[Type[Event], List[Callable[[Event], None]]]:
        d = defaultdict(list)
        for event, handler in handlers:
            d[event].append(handler)

        return dict(d)

    @singleton
    @provider
    def provide_event_bus(
        self, handlers: Dict[Type[Event], List[Callable[[Event], None]]]
    ) -> EventBus:
        return EventBus(handlers=handlers)
