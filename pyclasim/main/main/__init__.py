import logging
import os
import time
from os.path import expanduser
from typing import Any, Dict, List, Optional, Tuple

from injector import Injector

from main.di import setup_dependency_injection
from main.utils import get_settings, prepare_results_dir
from monitor import UpdatingMonitorsUseCase
from patient_models import PatientCondition
from patient_models.pulse import PulseFacade
from presentation import InputHandler

__all__ = [
    "create_simulation",
    "get_settings",
]

logger = logging.getLogger("main")


class Simulation:
    def __init__(
        self,
        injector: Injector,
        settings: Dict[str, Any],
        patient_conditions: Optional[List[PatientCondition]] = None,
    ) -> None:
        self._injector = injector
        self._settings = settings
        self.patient_conditions = patient_conditions

        self._initialize()

    def _initialize(self) -> None:
        self.pulse_facade = self._injector.get(PulseFacade)
        self.input_handler = self._injector.get(InputHandler)
        self.updating_monitors_uc = self._injector.get(UpdatingMonitorsUseCase)

    def run(self, reset_results_dir: bool) -> None:
        self._prepare_results_dir(reset_results_dir)

        self.pulse_facade.initialize_patient(patient_conditions=self.patient_conditions)
        self._run_simulation_loop(*self._extract_simulation_settings(self._settings))

    def _prepare_results_dir(self, reset_results_dir: bool) -> None:
        results_dir = expanduser(os.path.sep.join(self._settings["results_path"]))
        prepare_results_dir(results_dir, reset_results_dir)

    def _run_simulation_loop(
        self, simulation_timesteps: int, duration_per_timestep: int
    ) -> None:
        for timestep in range(simulation_timesteps):
            start_time = time.time()
            self.input_handler.process_input()
            self.updating_monitors_uc.execute(timestep)
            self.pulse_facade.advance(timestep)
            self._delay_update(start_time, duration_per_timestep)

    @staticmethod
    def _extract_simulation_settings(settings: Dict[str, Any]) -> Tuple[int, int]:
        simulation_duration = settings["simulation"]["duration"]
        duration_per_timestep = settings["simulation"]["duration_per_timestep"]
        timesteps = int(simulation_duration / duration_per_timestep)
        return timesteps, duration_per_timestep

    @staticmethod
    def _delay_update(start_time: float, duration_per_timestep: int) -> None:
        remaining_time = start_time + duration_per_timestep - time.time()

        if remaining_time > 0:
            time.sleep(remaining_time)


def create_simulation(
    settings_override: Optional[Dict[str, Any]] = None,
    patient_conditions: Optional[List[PatientCondition]] = None,
) -> Simulation:
    settings = get_settings()

    if settings_override is not None:
        settings.update(settings_override)

    dependency_injector = setup_dependency_injection(settings)
    return Simulation(dependency_injector, settings, patient_conditions)
