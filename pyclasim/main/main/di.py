from typing import Any, Dict

from injector import Injector
from messaging import MessagingModule

from main.modules import Configurations, EventBusModule, MessageBusModule
from monitor import MonitorInfrastructureModule, MonitorModule
from notifications import NotificationsModule
from patient_models.pulse import PulseModule
from presentation import PresentationModule
from pump import PumpInfrastructureModule, PumpModule


def setup_dependency_injection(settings: Dict[str, Any]) -> Injector:
    return Injector(
        [
            EventBusModule(),
            MessageBusModule(),
            Configurations(settings),
            PumpModule(),
            PumpInfrastructureModule(),
            MonitorModule(),
            MonitorInfrastructureModule(),
            PulseModule(),
            PresentationModule(),
            NotificationsModule(settings),
            MessagingModule(settings),
        ],
        auto_bind=False,
    )
