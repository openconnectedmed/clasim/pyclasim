import logging
import os
import shutil
from os.path import dirname, join
from typing import Any, Dict

import yaml

logger = logging.getLogger(__name__)


def get_settings(config_file: str = "config.yaml") -> Dict[str, Any]:
    base_dir = os.path.realpath(join(dirname(__file__), os.pardir, os.pardir))
    config_path = os.path.sep.join([base_dir, "resources", config_file])
    with open(config_path, "r") as file:
        settings = yaml.safe_load(file)

    return settings


def prepare_results_dir(results_dir: str, reset_results_dir: bool) -> None:
    if not os.path.exists(results_dir):
        logger.info(f"Creating new results directory, {results_dir}")
        os.makedirs(results_dir)

    else:
        if reset_results_dir:
            logger.info(f"Resetting results directory, {results_dir}")
            shutil.rmtree(results_dir)
            os.makedirs(results_dir)

        else:
            logger.info(f"Using existing results directory, {results_dir}")
