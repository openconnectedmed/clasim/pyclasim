from setuptools import find_packages, setup

setup(
    name="main",
    version="0.0.1",
    packages=find_packages(include=["main", "main.*"]),
    install_requires=[
        "injector",
        "pyyaml",
        "foundation",
        "messaging",
        "algorithms",
        "patient_models",
        "monitor",
        "pump",
        "presentation",
        "notifications",
    ],
    python_requires=">=3.7",
)
