from setuptools import find_packages, setup

setup(
    name="algorithms",
    version="0.0.1",
    packages=find_packages(include=["algorithms", "algorithms.*"]),
    python_requires=">=3.8",
)
