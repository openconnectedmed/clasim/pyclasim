from typing import Any, Dict, Type

from algorithms.base import AbstractAlgorithm
from algorithms.config import AlgorithmConfig
from algorithms.hypotension import HypotensionManagement
from algorithms.interfaces import Monitor, Pump


class AlgorithmFactory:

    _registry: Dict[str, Type[AbstractAlgorithm]] = {
        "hypotension": HypotensionManagement
    }

    @classmethod
    def create_algorithm(
        cls, name: str, pump: Pump, monitor: Monitor, settings: Dict[str, Any]
    ) -> AbstractAlgorithm:
        if not (algorithm_cls := cls._registry.get(name)):
            return ValueError(f"Unknown algorithm, '{name}'")

        return algorithm_cls(
            pump=pump,
            monitor=monitor,
            config=AlgorithmConfig(**settings[name]),
        )
