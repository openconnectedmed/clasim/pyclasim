import logging
import time
from typing import Callable

from algorithms.base import AbstractAlgorithm
from algorithms.config import AlgorithmConfig
from algorithms.interfaces import Monitor, Pump

logger = logging.getLogger("hypotension_management")


class HypotensionManagement(AbstractAlgorithm):
    def __init__(self, pump: Pump, monitor: Monitor, config: AlgorithmConfig) -> None:
        self.pump = pump
        self.monitor = monitor
        self.config = config
        self.infusion_rate: float = self.config.max_infusion_rate
        self.state: Callable[[int, float], None] = self._initialize_infusions
        self.next_update_time_s: int = 0

    def run(self) -> None:
        logger.info("hypotension management algorithm has started running...")

        try:
            while True:
                if data := self.monitor.get_patient_data():
                    current_sim_time_s = int(data["SimulationTime"])
                    mean_arterial_pressure = data[self.config.target_variable]
                    self.state(current_sim_time_s, mean_arterial_pressure)

                # No need to run at full speed since the simulation runs in real-time
                # (i.e. updates every 1s). Therefore, polling patient data evey half a
                # second seems reasonable
                time.sleep(0.5)

        except KeyboardInterrupt:
            logger.error("hypotension management algorithm interrupted")

    def _initialize_infusions(
        self, current_sim_time_s: int, mean_arterial_pressure: float
    ) -> None:
        self.pump.start(self.infusion_rate)
        self.next_update_time_s = current_sim_time_s + self.config.long_wait_s
        self.state = self._wait_for_MAP_increase

    def _wait_for_MAP_increase(
        self, current_sim_time_s: int, mean_arterial_pressure: float
    ) -> None:
        if not current_sim_time_s >= self.next_update_time_s:
            return

        if mean_arterial_pressure > self.config.max_target_range:
            self.infusion_rate *= self.config.percent_of_rate
            self.pump.set_rate(self.infusion_rate)
            self.next_update_time_s = current_sim_time_s + self.config.long_wait_s
            self.state = self._ensure_MAP_decline_after_increase

        else:
            self.next_update_time_s = current_sim_time_s + self.config.short_wait_s

    def _ensure_MAP_decline_after_increase(
        self, current_sim_time_s: int, mean_arterial_pressure: float
    ) -> None:
        if not current_sim_time_s >= self.next_update_time_s:
            return

        if mean_arterial_pressure > self.config.max_target_range:
            self.infusion_rate *= self.config.percent_of_rate
            self.pump.set_rate(self.infusion_rate)
            self.next_update_time_s = current_sim_time_s + self.config.long_wait_s

        else:
            self.next_update_time_s = current_sim_time_s + self.config.long_wait_s
            self.state = self._maintain_MAP_in_range

    def _maintain_MAP_in_range(
        self, current_sim_time_s: int, mean_arterial_pressure: float
    ) -> None:
        if not current_sim_time_s >= self.next_update_time_s:
            return

        if mean_arterial_pressure > self.config.max_target_range:
            self.infusion_rate *= self.config.percent_of_rate
            self.pump.set_rate(self.infusion_rate)
            self.next_update_time_s = current_sim_time_s + self.config.long_wait_s

        elif mean_arterial_pressure < self.config.min_target_range:
            self.infusion_rate = (
                self.config.max_infusion_rate * self.config.percent_of_rate
            )
            self.pump.set_rate(self.infusion_rate)
            self.next_update_time_s = current_sim_time_s + self.config.short_wait_s

        else:
            self.next_update_time_s = current_sim_time_s + self.config.long_wait_s
