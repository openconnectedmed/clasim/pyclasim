from typing import Dict, Protocol


class Monitor(Protocol):
    def get_patient_data(self) -> Dict[str, float]:
        ...
