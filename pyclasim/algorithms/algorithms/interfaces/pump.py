from typing import Protocol


class Pump(Protocol):
    def start(self, rate: float) -> None:
        ...

    def set_rate(self, rate: float) -> None:
        ...

    def stop(self) -> None:
        ...

    def pause(self) -> None:
        ...

    def unpause(self) -> None:
        ...
