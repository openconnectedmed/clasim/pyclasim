from abc import ABC, abstractmethod


class AbstractAlgorithm(ABC):
    @abstractmethod
    def run(self) -> None:
        pass
