from dataclasses import dataclass


@dataclass(frozen=True)
class AlgorithmConfig:
    target_variable: str
    max_infusion_rate: float
    max_target_range: float
    min_target_range: float
    percent_of_rate: float
    long_wait_s: int
    short_wait_s: int
