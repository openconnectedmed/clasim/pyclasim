from setuptools import find_packages, setup

setup(
    name="messaging",
    version="0.0.1",
    packages=find_packages(include=["messaging", "messaging.*"]),
    install_requires=[
        "injector",
        "pyzmq",
    ],
    python_requires=">=3.8",
)
