import abc
from dataclasses import dataclass
from typing import Optional


@dataclass
class BaseMessaging(abc.ABC):
    host: str
    port: str


class MessagingListener(BaseMessaging):
    @abc.abstractmethod
    def receive(self) -> Optional[str]:
        pass


class MessagingPublisher(BaseMessaging):
    @abc.abstractmethod
    def publish(self, data: str) -> None:
        pass


class MessagingResponder(BaseMessaging):
    @abc.abstractmethod
    def send(self, data: str, timeout: float) -> None:
        pass

    @abc.abstractmethod
    def receive(self, timeout: float) -> Optional[str]:
        pass


class MessagingRequester(MessagingResponder):
    pass


class MessagingResponder(MessagingResponder):
    pass
