from dataclasses import dataclass

import zmq
from messaging.zmq.base import AbstractZMQPublisher
from messaging.zmq.mixins import BindToEndPointMixin


@dataclass
class ZMQPublisher(BindToEndPointMixin, AbstractZMQPublisher):
    def __post_init__(self) -> None:
        self._initialize()

    def _initialize(self) -> None:
        context = zmq.Context()
        self.publisher = context.socket(zmq.PUB)
        self.bind_socket_to_endpoint(self.publisher, f"tcp://{self.host}:{self.port}")

    def publish(self, data: str, channel: str = None) -> None:
        if channel is not None:
            self.channel = channel

        self.publisher.send_multipart([self.channel.encode(), data.encode()])
