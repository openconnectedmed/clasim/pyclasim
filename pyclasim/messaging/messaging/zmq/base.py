from dataclasses import dataclass

from messaging.base import MessagingListener, MessagingPublisher


@dataclass
class AbstractZMQListener(MessagingListener):
    channel: str


@dataclass
class AbstractZMQPublisher(MessagingPublisher):
    channel: str
