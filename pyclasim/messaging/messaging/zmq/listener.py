from dataclasses import dataclass, field
from typing import Optional

import zmq
from messaging.zmq.base import AbstractZMQListener
from messaging.zmq.mixins import BindToEndPointMixin


@dataclass
class ZMQListener(BindToEndPointMixin, AbstractZMQListener):
    _poller: zmq.Poller = field(init=False, default=zmq.Poller())

    def __post_init__(self) -> None:
        self._initialize()

    def _initialize(self) -> None:
        context = zmq.Context()
        self.subscriber = context.socket(zmq.SUB)
        self.subscriber.setsockopt(zmq.SUBSCRIBE, self.channel.encode())
        self.bind_socket_to_endpoint(self.subscriber, f"tcp://{self.host}:{self.port}")

        self._poller.register(self.subscriber, zmq.POLLIN)

    def receive(self, timeout: float = 0) -> Optional[str]:
        socks = dict(self._poller.poll(timeout))
        if not socks.get(self.subscriber):
            return

        if socks[self.subscriber] == zmq.POLLIN:
            (_, contents) = self.subscriber.recv_multipart()
            return contents.decode()
