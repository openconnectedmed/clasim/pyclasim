from typing import Any, Dict

from messaging import (
    MessagingListener,
    MessagingPublisher,
    MessagingRequester,
    MessagingResponder,
)
from messaging.base_factory import MessagingFactory
from messaging.zmq.listener import ZMQListener
from messaging.zmq.publisher import ZMQPublisher
from messaging.zmq.requester import ZMQRequester
from messaging.zmq.responder import ZMQResponder


class ZMQMessagingFactory(MessagingFactory):
    @classmethod
    def create_publisher(cls, settings: Dict[str, Any]) -> MessagingPublisher:
        return ZMQPublisher(
            host=settings["host"],
            port=settings["port"],
            channel=settings["channel"],
            bind_to_endpoint=settings["bind_to_endpoint"],
        )

    @classmethod
    def create_listener(cls, settings: Dict[str, Any]) -> MessagingListener:
        return ZMQListener(
            host=settings["host"],
            port=settings["port"],
            channel=settings["channel"],
            bind_to_endpoint=settings["bind_to_endpoint"],
        )

    @classmethod
    def create_responder(cls, settings: Dict[str, Any]) -> MessagingResponder:
        return ZMQResponder(
            host=settings["host"],
            port=settings["port"],
        )

    @classmethod
    def create_requester(cls, settings: Dict[str, Any]) -> MessagingRequester:
        return ZMQRequester(
            host=settings["host"],
            port=settings["port"],
        )
