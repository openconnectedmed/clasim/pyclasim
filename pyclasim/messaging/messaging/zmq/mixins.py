import time
from dataclasses import dataclass
from typing import Optional

import zmq


@dataclass
class BindToEndPointMixin:
    bind_to_endpoint: Optional[bool] = False

    def bind_socket_to_endpoint(self, socket: zmq.Socket, endpoint: str) -> None:
        if self.bind_to_endpoint:
            socket.bind(endpoint)
        else:
            socket.connect(endpoint)
        time.sleep(0.2)


@dataclass
class ZMQReqRepMixin:
    def _receive(self, timeout: float) -> Optional[str]:
        socks = dict(self._poller.poll(timeout))
        if not (self.socket in socks and socks[self.socket] == zmq.POLLIN):
            return

        response = self.socket.recv()
        return response.decode()

    def _send(self, data: str, timeout: float) -> None:
        socks = dict(self._poller.poll(timeout))
        if not (self.socket in socks and socks[self.socket] == zmq.POLLOUT):
            return

        self.socket.send(data.encode())
