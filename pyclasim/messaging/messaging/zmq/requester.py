import time
from dataclasses import dataclass, field
from typing import Optional

import zmq
from messaging import MessagingRequester
from messaging.zmq.mixins import ZMQReqRepMixin


@dataclass
class ZMQRequester(ZMQReqRepMixin, MessagingRequester):
    _poller: zmq.Poller = field(init=False, default=zmq.Poller())

    def __post_init__(self) -> None:
        self._initialize()

    def _initialize(self) -> None:
        context = zmq.Context()
        self.socket = context.socket(zmq.REQ)
        self.socket.connect(f"tcp://{self.host}:{self.port}")
        time.sleep(0.2)

        self._poller.register(self.socket, zmq.POLLIN | zmq.POLLOUT)

    def receive(self, timeout: float = 0) -> Optional[str]:
        response = self._receive(timeout)
        return response

    def send(self, data: str, timeout: float = 0) -> None:
        self._send(data, timeout)
