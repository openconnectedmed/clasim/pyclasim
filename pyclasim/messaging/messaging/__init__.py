from typing import Type

from injector import Module, provider, singleton
from messaging.base import (
    MessagingListener,
    MessagingPublisher,
    MessagingRequester,
    MessagingResponder,
)
from messaging.base_factory import MessagingFactory
from messaging.zmq import ZMQMessagingFactory

__all__ = [
    "MessagingListener",
    "MessagingPublisher",
    "MessagingResponder",
    "MessagingRequester",
    "ZMQMessagingFactory",
]


class MessagingModule(Module):
    def __init__(self, settings) -> None:
        self._settings = settings["messaging"]["backend"]
        self._messaging_type = settings["messaging"]["type"]

    @provider
    def provide_factory_cls(self) -> Type[MessagingFactory]:
        if self._messaging_type == "zmq":
            return ZMQMessagingFactory

    @singleton
    @provider
    def provide_listener(
        self, factory_cls: Type[MessagingFactory]
    ) -> MessagingListener:
        return factory_cls.create_listener(
            self._settings[self._messaging_type]["listener"]
        )

    @singleton
    @provider
    def provide_responder(
        self, factory_cls: Type[MessagingFactory]
    ) -> MessagingResponder:
        return factory_cls.create_responder(
            self._settings[self._messaging_type]["responder"]
        )

    @singleton
    @provider
    def provide_publisher(
        self, factory_cls: Type[MessagingFactory]
    ) -> MessagingPublisher:
        return factory_cls.create_publisher(
            self._settings[self._messaging_type]["publisher"]
        )
