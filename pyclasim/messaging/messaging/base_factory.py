import abc
from typing import Any, Dict

from messaging import (
    MessagingListener,
    MessagingPublisher,
    MessagingRequester,
    MessagingResponder,
)


class MessagingFactory(abc.ABC):
    @abc.abstractclassmethod
    def create_publisher(cls, settings: Dict[str, Any]) -> MessagingPublisher:
        pass

    @abc.abstractclassmethod
    def create_listener(cls, settings: Dict[str, Any]) -> MessagingListener:
        pass

    @abc.abstractclassmethod
    def create_responder(cls, settings: Dict[str, Any]) -> MessagingResponder:
        pass

    @abc.abstractclassmethod
    def create_requester(cls, settings: Dict[str, Any]) -> MessagingRequester:
        pass
