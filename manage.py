import logging
import subprocess

import click
import zmq

logging.basicConfig(
    level=logging.INFO,
    format="%(asctime)s %(levelname)s %(message)s",
)

LIBS = (
    "pyclasim/foundation",
    "pyclasim/messaging",
    "pyclasim/algorithms",
    "pyclasim/infrastructure",
    "pyclasim/pump",
    "pyclasim/notifications",
    "pyclasim/patient_models",
    "pyclasim/patient_conditions",
    "pyclasim/monitor",
    "pyclasim/presentation",
    "pyclasim/main",
    "pyclasim/cli",
)


@click.group()
def cli():
    pass


@cli.command()
def install_deps():
    for lib in LIBS:
        subprocess.call(f"pip install -e {lib}", shell=True)


@cli.command()
def freeze_deps():
    lib_deps = " ".join([lib + "/requirements.in" for lib in LIBS])
    subprocess.call("pip install pip-tools==5.5.0", shell=True)
    subprocess.call(
        f"pip-compile --no-annotate -o requirements.txt {lib_deps}", shell=True
    )


@cli.command()
@click.option("--frontend-port", type=int, default=5590)
@click.option("--backend-port", type=int, default=5584)
def start_zmq_proxy(frontend_port, backend_port):
    logging.info("zmq proxy is running...")

    context = zmq.Context()

    try:
        frontend = context.socket(zmq.XSUB)
        frontend.bind(f"tcp://*:{frontend_port}")

        backend = context.socket(zmq.XPUB)
        backend.bind(f"tcp://*:{backend_port}")

        zmq.proxy(frontend, backend)

    except KeyboardInterrupt:
        logging.error("zmq proxy interrupted")

    finally:
        frontend.close()
        backend.close()
        context.term()


if __name__ == "__main__":
    cli()
